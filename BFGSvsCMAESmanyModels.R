nTest <- 100
allSolBFGS <- allSolCMAES <- rep(0, nTest)
for (k in 1:nTest) {
  source("generateModel.R")
  allSolBFGS[k] <- BFGSoptimForR(bList, data, n, p, q, h, boolInter, isFlow,
                           numericalGradientStep = 1e-9, numBasis = 5, maxIterations = 10000,  
                           armijoConstant = 1e-4, wolfe = 0.9, minGradientNorm = 1e-9, factr = 1e-9, 
                           maxLineSearchTrials = 100, minStep = 1e-20, maxStep = 1e20)$value
  
  allSolCMAES[k] <- CMAESoptimForR(bList, data, n, p, q, h, boolInter, isFlow,
                             lambda <- 2*floor(4+3*log(length(bList$b))), # use <- and not = here, as we need lambda to define nIterMax
                             sigma = 0.2, nIterMax = 200 + 50*(sum(bList$bPos)+3)**2/sqrt(lambda),
                             tol = 1e-9)$value
}

nCMAESwins <- length(which(round(allSolCMAES, 3)<round(allSolBFGS, 3)))
nEquality <- length(which(round(allSolCMAES, 3)==round(allSolBFGS, 3)))
nBFGSwins <- nTest - nCMAESwins - nEquality

cat("\n proportion of equalities:                                  ", nEquality/nTest*100, "%\n", 
    "proportion of cases where cmaes wins (igoring equalities): ", (nCMAESwins)/(nTest-nEquality)*100, "%\n", 
    "proportion of cases where BFGS wins (igoring equalities):  ", (nBFGSwins)/(nTest-nEquality)*100, "%\n", 
    "(result were obtained by running", nTest, "simulations)\n")
