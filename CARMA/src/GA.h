# ifndef GA_H_DEFINED
# define GA_H_DEFINED

#define ARMA_DONT_PRINT_ERRORS
#include <RcppArmadillo.h>

/*****************************************************************/
/*****************CLASS FOR  GENETIC ALGORITHMS*******************/
/***********  Performs optimization on a binary vector ***********/
/*****************************************************************/ 

template <typename T1>
class GA
{
public:
  // CONSTRUCTOR //
  GA(T1 toOptimizeIn,
  	 const size_t populationSizeIn = 100,
  	 const size_t maxGenerationsIn = 1000,
  	 const double mutationProbIn = 0.1,
  	 const double selectPercentIn = 0.1,
  	 const double toleranceIn = 1e-3) :
    toOptimize(toOptimizeIn),
    populationSize(populationSizeIn), 
    maxGenerations(maxGenerationsIn), mutationProb(mutationProbIn), 
    selectPercent(selectPercentIn), tolerance(toleranceIn) { }
  
  void Mutate(std::vector<arma::Col<unsigned int>>& population,
              arma::uvec& index) {
    unsigned int bInitSize = population[0].n_elem;
    // Mutate all elements of the population according to the given probability of mutation
    // The best candidate is not altered.
    for (size_t i = 1; i < populationSize; i++) {
	  	arma::vec randForMutate = arma::randu(bInitSize);
		for (size_t j=0; j<bInitSize; ++j) {
			if (randForMutate[j]<mutationProb) {
				population[index(i)](j) = 1 - population[index(i)](j); 
			}
		}
    }
  }
  
  void Crossover(std::vector<arma::Col<unsigned int>>& population,
                 const size_t mom,
                 const size_t dad,
                 const size_t child1,
                 const size_t child2) {
    // Replace the candidates with parents at their place.
    population[child1] = population[mom];
    population[child2] = population[dad];
    
    // Randomly alter mom and dad genome weights to get two different children.
    for (size_t i = 0; i < population[mom].n_elem; i++) {
      // Using it to alter the weights of the children.
      const double random = arma::randu<double>();
      if (random > 0.5) {
        population[child1](i) = population[mom](i);
        population[child2](i) = population[dad](i);
      }
      else {
        population[child1](i) = population[dad](i);
        population[child2](i) = population[mom](i);
      }
    }
  }
  
  void Reproduce(std::vector<arma::Col<unsigned int>>& population,
                 const arma::vec& fitnessValues,
                 arma::uvec& index,
                 unsigned int numElite) {
    // Sort fitness values. Smaller fitness value means better performance.
    index = arma::sort_index(fitnessValues);
    // First parent.
    size_t mom;
    // Second parent.
    size_t dad;
    
    for (size_t i = numElite; i < populationSize - 1; i++) {
      // Select 2 different parents from elite group randomly [0, numElite).
      mom = arma::as_scalar(arma::randi<arma::uvec>(
        1, arma::distr_param(0, numElite - 1)));
      
      dad = arma::as_scalar(arma::randi<arma::uvec>(
        1, arma::distr_param(0, numElite - 1)));
      // Making sure both parents are not the same.
      if (mom == dad) {
        if (dad != numElite - 1) {
          dad++;
        }
        else {
          dad--;
        }
      }
      
      // Parents generate 2 children replacing the dropped-out candidates.
      // Also finding the index of these candidates in the population matrix.
      Crossover(population, index[mom], index[dad], index[i], index[i + 1]);
    }
    // Mutating the weights with small noise values.
    // This is done to bring change in the next generation.
    Mutate(population, index);
  }
  
  double Optimize(arma::Col<unsigned int>& bInit) {
    const unsigned int bInitSize = bInit.n_elem;
    arma::vec fitnessValues(populationSize);
    arma::uvec index;
    // Make sure for evolution to work at least four candidates are present.
    if (populationSize < 4) {
      throw std::logic_error("CNE::Optimize(): population size should be at least 4!");
    }
    
    // Find the number of elite canditates from population.
    unsigned int numElite = floor(selectPercent * populationSize);
    
    // Making sure we have even number of candidates to remove and create.
    if ((populationSize - numElite) % 2 != 0)
      numElite--;
    
    // Terminate if two parents can not be created.
    if (numElite < 2) {
      throw std::logic_error("CNE::Optimize(): unable to select two parents. "
                               "Increase selection percentage.");
    }
    // Terminate if at least two childs are not possible.
    if ((populationSize - numElite) < 2) {
      throw std::logic_error("CNE::Optimize(): no space to accomodate even 2 "
                               "children. Increase population size.");
    }
    // Generate the population based on a Gaussian distribution around the given
    // starting point.
    std::vector<arma::Col<unsigned int>> population;
    for (size_t i = 0 ; i < populationSize; ++i) {
      population.push_back(arma::conv_to<arma::Col<unsigned int>>::from(arma::randi(bInitSize, arma::distr_param(0, 1))));
    }
    
    // Controls early termination of the optimization process.
    bool terminate = false;
    // Find the fitness before optimization using given iterate parameters.
    double lastBestFitness = toOptimize.Evaluate(bInit);
  	for (size_t gen = 1; gen <= maxGenerations && !terminate; gen++) {
		// Calculating fitness values of all candidates.
	#pragma omp parallel for default(shared) firstprivate(toOptimize)
		for (size_t i = 0; i < populationSize; i++) {
			// Find fitness of candidate.
			fitnessValues[i] = toOptimize.Evaluate(population[i]);
			if (std::isnan(fitnessValues[i])) fitnessValues[i] = 1e50; //On Florent's computer there is sometimes a nan issue (that remains unexplained yet)...This should prevent it to happen.
		}
		// Create next generation of species.
		Reproduce(population, fitnessValues, index, numElite);
		if (std::abs(lastBestFitness - fitnessValues.min())<tolerance) { //test convergence condition
			Rcpp::Rcout<<"GA: Convergence Reached at Iteration: "<<gen<<std::endl;
			terminate = true;
		}
		// Store the best fitness of present generation.
		lastBestFitness = fitnessValues.min();
	}

    // Set the best candidate into the network parameters.
    bInit = population[index(0)];
  	return lastBestFitness;
  }
  
  
  
private:
	T1 toOptimize;
	const size_t populationSize;
	const size_t maxGenerations;
	const double mutationProb;
	const double selectPercent;
	const double tolerance;
};
#endif





