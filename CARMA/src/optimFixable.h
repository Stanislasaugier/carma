# ifndef CARMAFIXABLE_H_DEFINED

# define CARMAFIXABLE_H_DEFINED
#define ARMA_DONT_PRINT_ERRORS
#include <RcppArmadillo.h>
// T1 for carmaClass
// T1Fixed for carmaFixed
// T2 for optim
template<typename T1, typename T1Fixed, typename T2>
class optimFixable {
public:
  // CONSTRUCTOR
  optimFixable(T2 anOptimIn,
               T1* aCarmaModelIn,
               const arma::Col<unsigned int> bFixableIn, 
               const arma::vec bFullDefaultIn,
               const arma::vec bFullFixedIn, 
               const arma::Col<unsigned int> bFixedInitIn,
               const arma::vec bLowerIn,
               const arma::vec bUpperIn, 
               const std::string criterionTypeIn) :
  anOptim(anOptimIn),
  aCarmaModel(aCarmaModelIn),
  bFixable(bFixableIn),
  bFullDefault(bFullDefaultIn),
  bFullFixed(bFullFixedIn),
  bFixedInit(bFixedInitIn),
  nEltsBFixable(bFixableIn.n_elem),
  bLower(bLowerIn),
  bUpper(bUpperIn), 
  criterionType(criterionTypeIn) { }
  
  arma::Col<unsigned int> getBFixedFull(const arma::Col<unsigned int> bFixedPart) {
    arma::Col<unsigned int> out(bFixable.n_elem);
    unsigned int compt=0;
    for (unsigned int it=0; it<bFixable.n_elem; it++) {
      if (bFixable[it]==1) {
        out[it] = bFixedPart[compt];
        compt++;
      } else {
        out[it] = bFixedInit[it];
      }
    }
    return out;
  }
  
  double Evaluate(arma::Col<unsigned int>& bFixedPart) {
    arma::Col<unsigned int> bFixed = getBFixedFull(bFixedPart);
    // Generate bFullInit depending on which parameters are fixed and which are not
    arma::vec bFullInit = bFullDefault;
    for (unsigned int it=0; it<nEltsBFixable; it++) {
      if (bFixed[it]==0) {
        bFullInit[it] = bFullFixed[it];
      }
    }
    T1Fixed myT1Fixed(aCarmaModel, bFixed, bFullInit, bLower, bUpper);
    arma:vec bInit = myT1Fixed.bPartGet(bFullInit);
    anOptim.editModel(myT1Fixed);
    double LLK = anOptim.Optimize(bInit);
    if (criterionType=="AIC") {
      // Rcpp::Rcout<<"using AIC criterion"<<std::endl;
      return 2*bInit.n_elem + 2*LLK; //AIC Criterion
    }
    // Rcpp::Rcout<<"using BIC criterion"<<std::endl;
    return std::log(aCarmaModel->n*aCarmaModel->Time)*bInit.n_elem + 2*LLK;
  }
  
private:
  T2 anOptim;
  T1* aCarmaModel;
  const arma::Col<unsigned int> bFixable;
  const arma::vec bFullDefault;
  const arma::vec bFullFixed;
  const arma::Col<unsigned int> bFixedInit;
  const unsigned int nEltsBFixable;
  const arma::vec bLower;
  const arma::vec bUpper;
  const std::string criterionType;
};

#endif