# ifndef CMAES_H_DEFINED
# define CMAES_H_DEFINED
#define ARMA_DONT_PRINT_ERRORS
#include <RcppArmadillo.h>
#include <omp.h>
#include "carmaClass.h"
#include "carmaClassFixed.h"

using namespace arma;
/******************************/
/*  Optimization using CMAES  */
/******************************/

template<typename T>
class CMAES
{
public:
//Constructor
CMAES(T modelIn,
      const unsigned int lambdaIn,
      double sigmaIn, 
      const unsigned int nIterMaxIn, 
      const double tolIn,
  		const bool useParallelIn) : 
  model(modelIn),
    lambda(lambdaIn), sigmaMem(sigmaIn), nIterMax(nIterMaxIn),
    tol(tolIn), tolFitness(1e-3), useParallel(useParallelIn) {}

  // Minimizes (minus) the log-likelihood using CMA-ES
  double Optimize(arma::vec& m) {
    const unsigned int n = m.n_elem;
  	double sigma = sigmaMem;
    double mu = floor(lambda/2);
  	arma::vec w = createw(lambda, n);
  	double muEff = pow(accu(w(span(0, mu-1))), 2)/accu(pow(w(span(0, mu-1)), 2)); 
    double EN01 = std::pow(n, 0.5)*(1-1/(4*n)+1/(21*std::pow(n, 2)));
  	double csig = (muEff+2)/(n + muEff+5);
    double dsig = 1 + 2*std::max(0.0, std::pow((muEff-1)/(n+1), 0.5)-1) + csig;
    double cc = (4 + muEff/n)/(n + 4 + 2*muEff/n);
  	double c1 = 2/(std::pow(n + 1.3, 2) + muEff);
  	double cb = 1;
    double cmu = std::min(1-c1, 2*(muEff- 2 + 1/muEff)/(std::pow(2+n, 2) + 2*muEff/2));
    double hsig = 0;
  	unsigned int indexPastVal = 0;
  	unsigned int maxPastVal = 95 + floor(30*n/lambda);
  	unsigned int indexNewestVal = 0;
  	arma::vec NewestVal(25);
  	arma::vec OldestVal(25);
  
  	unsigned int nTrial;
  	arma::vec yw(n);
  	arma::mat C = eye(n, n);
    arma::mat zlambda(n, lambda);
    arma::mat ylambda(n, lambda);
    arma::mat xlambda(n, lambda);
    arma::vec flambda(lambda);
    arma::vec psig = zeros(n);
    arma::vec pc = zeros(n);
    arma::Col<unsigned int> flambdaIndex(lambda);
    arma::vec D(n);
    arma::mat B(n, n);
    arma::vec tempM;
  	arma::vec pastVal(maxPastVal);
  	arma::Col<unsigned int> pastAvgResults(n, fill::zeros);
  	arma::vec wo = w;
  	unsigned int indexPastAvgResults = 0;
  
  	// MAIN LOOP //
  	for (unsigned int i=0; i<nIterMax; ++i) {
  	  // GENERATE NEW GENERATION OF CANDIDATES //
  	  eig_sym(D, B, C);
  	  D = pow(D, 0.5);
  	  // We can't have a parallel construct nested in a single construct, need to start a new parallel regin at each iteration of the for loop
      #pragma omp parallel for default(shared) private(nTrial, tempM) if(useParallel)
  	  for (unsigned int j=0; j<lambda; ++j) { //Generate offsprings and compute their fitness
    		nTrial = 0;
    		do {
    			zlambda.col(j) = randn(n);
    			ylambda.col(j) = B*diagmat(D)*zlambda.col(j);
    			xlambda.col(j) = m + sigma*ylambda.col(j);
    			tempM = vec(xlambda.colptr(j), n, false, false);
    			flambda(j) = model.Evaluate(tempM);
    			nTrial++;
    		} while (flambda(j)>=1e50 && nTrial<100); // Re-generate points untill the LLK is successfully computed
  	  }
  	  flambdaIndex = arma::sort_index(flambda, "ascend"); // sort points by fitness
  	  yw = ylambda.cols(flambdaIndex(arma::span(0, mu-1)))*w(span(0, mu-1));
  	  // SELECTION AND RECOMBINATION
  	  m = m + cb*sigma*yw; //update m taking the mean of the selected points
  	  // STEP-SIZE CONTROL
  	  psig = (1 - csig)*psig + std::pow(csig*(2-csig)*muEff, 0.5)*B*zlambda.cols(flambdaIndex(span(0, mu-1)))*w(span(0, mu-1));
  	  sigma = sigma*exp(csig/dsig*(norm(psig)/EN01-1));
  	  //COVARIANCE MATRIX ADAPTATION
  	  for (unsigned int j=mu;j<lambda; ++j) {
    		wo(j)= w(j)*n/pow(norm(B*zlambda.col(flambdaIndex(j)), 2), 2);
  	  }
  	  hsig = ((norm(psig)/std::pow(1 - std::pow(1.0-csig, 2.0*(n+1.0)), 0.5)) < ((1.4 + 2/(n+1))*EN01)) ? 1.0 : 0.0;
  	  pc = (1-cc)*pc + hsig*std::pow(cc*(2-cc)*muEff, 0.5)*ylambda.cols(flambdaIndex(arma::span(0, mu-1)))*w(span(0, mu-1));
  	  C = (1 + c1*(1-hsig)*cc*(2-cc) - c1 - cmu*accu(wo))*C + c1*pc*pc.t() + cmu*ylambda.cols(flambdaIndex)*diagmat(wo)*ylambda.cols(flambdaIndex).t();
  	  C = trimatu(C) + trimatl(C, -1); //Enforce symmetry of C
      //if (std::abs((flambda(flambdaIndex(0))-flambda(flambdaIndex(mu-1)))/flambda(flambdaIndex(0)))<tolFitness) { //check for flatness
      //  sigma = sigma*exp(0.2+csig/dsig);
      //  Rcpp::Rcout<<"Warning: Flat fitness function"<<std::endl;
      //}
  	  //TERMINATION CRITERIA
  	  if (max(pc)<tol && max(sigma*C.diag())<tol) {
  		if (useParallel==true) Rcpp::Rcout<<"Terminating successfully: pc and C's values are below threshold, at iteration: "<<i<<std::endl;
  	    break;
  	  }
  	  if (max(D)>pow(10, 14)*min(D)) {
  	    if (useParallel==true) Rcpp::Rcout<<"Terminating successfully: Excessive condition number of the covariance matrix, at iteration: "<<i<<std::endl;
  	    break;
  	  }
  	  (indexPastVal<maxPastVal-1) ? indexPastVal++ : indexPastVal = 0;
  	  (indexNewestVal<25-1) ? indexNewestVal++ : indexNewestVal=0;
  	  OldestVal(indexNewestVal) = pastVal(indexPastVal);
  	  pastVal(indexPastVal) = model.Evaluate(m);
  	  NewestVal(indexNewestVal) = pastVal(indexPastVal);
  	  if (i>maxPastVal+25 && std::abs(mean(OldestVal) - mean(NewestVal))<tol & std::abs(median(OldestVal) - median(NewestVal))<tol) {
    		if (useParallel==true) Rcpp::Rcout<<"Terminating successfully: Stagnation of function value at m, at iteration: "<<i<<std::endl;
  	  	break;
  	  }
  	  (indexPastAvgResults<n-1) ? indexPastAvgResults++ : indexPastAvgResults = 0;
  	  flambda(flambdaIndex(mu-1)) - flambda(flambdaIndex(0))<tol ? pastAvgResults(indexPastAvgResults)=1 : pastAvgResults(indexPastAvgResults) = 0;
  	  if (accu(pastAvgResults)>n/3) {
        if (useParallel==true) Rcpp::Rcout<<"Terminating successfully: Constant value for all best mu points, at iteration: "<<i<<std::endl;
        break;
  	 	}
  	  if (norm(0.1*sigma*D(n-1)*B.col(n-1)/m)<tol) {
        if (useParallel==true) Rcpp::Rcout<<"Terminating successfully: A shock on the principal axis of C does not significantly change m, at iteration: "<<i<<std::endl;
    	  break;
  	  }
      //Rcpp::Rcout<<"generation: "<<i<<"flambda: "<<flambda(flambdaIndex(0))<<" "<<flambda(flambdaIndex(mu-1))<<" "<<pastVal(indexPastVal)<<" " <<max(abs(m))<<std::endl;
  	  if (flambda(flambdaIndex(mu-1))>1e45) break;
  	}
  	
  	// generate and return output (best solution m and value at the best solution)
    return pastVal(indexPastVal);
  }
  
  void editModel(T& newModel) {
    model = newModel;
  }

private:
  T model; // a class containing a member function double Evaluate(arma::vec) to minimize
  const unsigned int lambda; 
  double sigmaMem; // used to reset sigma between two calls of the function with he genetic algorithm
  const unsigned int nIterMax;
  const double tol;
  const double tolFitness;
  const bool useParallel;
  const bool destroyPointers = false;
	arma::vec createw(const unsigned int LAMBDA, const unsigned int N) { // used to initialize w
		const unsigned int MU = floor(LAMBDA/2);
		arma::vec out(LAMBDA);
		for (unsigned int i=0; i<LAMBDA; ++i) {
      		out(i) = log((LAMBDA+1)/2) - log(i+1);
    	}
		double MUEFF = pow(accu(out(span(0, MU-1))), 2)/accu(pow(out(span(0, MU-1)), 2));
		double MUEFFminus = pow(accu(out(span(MU, LAMBDA-1))), 2)/accu(pow(out(span(MU, LAMBDA - 1)), 2));
		double C1 = 2/(std::pow(N + 1.3, 2) + MUEFF);
		double CC = (4 + MUEFF/N)/(N + 4 + 2*MUEFF/N);
		double CMU = std::min(1-C1, 2*(MUEFF- 2 + 1/MUEFF)/(std::pow(2+N, 2) + 2*MUEFF/2));
		double ALPHAMUminus = 1 + C1/CMU;
		double ALPHAMUEFFminus = 1 + 2*MUEFFminus/(MUEFF+2);
		double ALPHAPOSDEFminus = (1 - C1 - CMU)/(N*CMU);
		double SUMTEMPplus = accu(out(span(0, MU-1)));
		double SUMTEMPminus = std::abs(accu(out(span(MU, LAMBDA-1))));
		for (unsigned int i = 0; i<MU;++i) {
			out(i) *= 1/SUMTEMPplus;
		}
		for (unsigned int i = MU; i<LAMBDA;++i) {
			out(i) *= std::min(std::min(ALPHAMUminus, ALPHAMUEFFminus), ALPHAPOSDEFminus)/SUMTEMPminus;
		}
		return out;
  	}
};
#endif
