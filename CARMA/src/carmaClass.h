# ifndef CARMALLK_H_DEFINED

# define CARMALLK_H_DEFINED
#define ARMA_DONT_PRINT_ERRORS
#include <RcppArmadillo.h>


class carmaModel
{
public:
  // CONSTRUCTOR
  carmaModel(const unsigned int nIn,
             const unsigned int pIn,
             const unsigned int qIn,
             const unsigned int nfIn,
             const double hIn,
             const bool boolInterIn,
             const arma::Col<unsigned int> & stocksIn,
             const arma::Col<unsigned int> & flowsIn, 
             const arma::mat & xTh);
  
  // Alternate Constructor, does not require any data as input
  // Used to write discrete time representation of a continuous time model.
  carmaModel(const unsigned int nIn,
             const unsigned int pIn,
             const unsigned int qIn,
             const unsigned int nfIn,
             const double hIn,
             const bool boolInterIn,
             const arma::Col<unsigned int> & stocksIn,
             const arma::Col<unsigned int> & flowsIn);
  
  // Copy Constructor
  carmaModel(const carmaModel& carmaModelIn);

  // Create the matrix H from Thornton and Chambers, 2017, equation (8). Does not take the exponential of it  
  arma::mat createAugH(const arma::vec bFull);
  // Create the vector a from Thornton and Chambers, 2017, equation (8)
  arma::vec createAuga (const arma::vec bFull);
  // Create the matrix theta from Thornton and Chambers, 2017, equation (8)
  arma::mat createAugTheta (const arma::vec bFull);
  //Creates the matrix sigma of the continous time model, makes sure that the resulting matrix is symmetric
  arma::mat createSigma (const arma::vec bFull);
  // Computes the exact discrete time representation from Thornton and Chambers (2017), theorems 1 and 2.
  void cppEDRh(const arma::vec bFull, arma::mat& bigF, arma::mat& f, arma::mat& covv);

  /***************************************************/
  /********** COMPUTATION OF THE LIKELIHOOD **********/
  /***************************************************/
  
  // Computes the Choleski decomposition of a Toeplitz block matrix
  void blockCholCpp(arma::mat& omega, unsigned int T, arma::mat& U, arma::mat& Uinv);
  // Computes (minus) the log likelihood using the exact discret time representation and the data contained in bStruct. 
  // may return an error.
  double LLK(const arma::vec bFull);
  double LLKbounded(const arma::vec bFull);
  // Try to compute the likelihood using LLK but returns a large value (1e50) 
  // if an error occured(typically if the choleski decomposition fails)
  double Evaluate(const arma::vec bFull);

  double penalize(const arma::vec bFull, const arma::vec bFullInit);
  
  const unsigned int n;
  const unsigned int p;
  const unsigned int q;
  const unsigned int nf;
  const double h;
  const bool boolInter;
  const arma::Col<unsigned int> stocks;
  const arma::Col<unsigned int> flows;
  const unsigned int r;
  const unsigned int ns;
  const unsigned int m;
  const unsigned int Time;
private:
  // Defines the matrix S1, such that the stock variables are first.
  arma::mat createS1();
  // Defines the matrix S2, such that the stock variables are first.
  arma::mat createS2 ();
  // Define the matrix N from Thornton and Chambers (2017).
  arma::mat createN(arma::mat& c11, arma::mat& c21);
  
  // Define the matrix N from Thornton and Chambers (2017).
  // Does not invert it or multiply it by T as in the original R code, which is made in the EDRh function
  arma::mat createM(arma::mat& c12, arma::mat& c22);
  // Define the matrix R (named T in the code) from Thornton and Chambers (2017). 
  //It is the matrix that spans the nullspace of N
  arma::mat createT(arma::mat& c11, arma::mat& c21);
  // Computes the vector epsilon from from Thornton and Chambers (2017), equation (10).
  arma::mat phiMat(const arma::mat& H);
  //Computes the matrix bigC = exp(H) with a minimum computation cost. Save intermediary matrices to be used again for omegaEpsMat
  arma::mat myExpmatForH(arma::mat J, const unsigned int dimH, const arma::uword s,
						 std::vector<arma::mat>& allPowH, std::vector<arma::mat>& allPowExpA);
  // Computes the matrix omega epsilon from from Thornton and Chambers (2017), theorem 1.
//  arma::mat omegaEpsMat(arma::mat H, const arma::mat & theta, const arma::mat & sigma);
  arma::mat omegaEpsMat(arma::mat J, const unsigned int dimH, const arma::uword s,
						std::vector<arma::mat>& allPowH, std::vector<arma::mat>& allPowExpA);
  // Transformation of data structure in a format manageable by other functions  
  arma::mat dataBlockCpp(const arma::mat& xTh);
  /***************************************************/
  /********** CONSTANT PARAMETERS AND DATA ***********/
  /***************************************************/
   
  arma::mat S1;
  arma::mat S2;
  arma::mat x;
};
#endif