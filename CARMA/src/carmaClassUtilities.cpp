#define ARMA_DONT_PRINT_ERRORS
#include <RcppArmadillo.h>
#include <omp.h>
#include "carmaClass.h"
#include "carmaClassFixed.h"
#include "BFGS.h"
#include "CMAES.h"
#include "optimFixable.h"
#include "GA.h"

Rcpp::List EDRhforR(const arma::vec bFull, // Computes the exact discrete time representation from Thornton and Chambers (2017), theorems 1 and 2.
                    const unsigned int n,
                    const unsigned int p,
                    const unsigned int q,
                    const double h,
                    const bool boolInter,
                    arma::Col<unsigned int> isFlow) {
  //Set stocks and flows
  unsigned int nf = accu(isFlow);
  unsigned int ns = n - nf;
  arma::Col<unsigned int> stocks(ns), flows(nf);
  unsigned int comptStocks = 0, comptFlows = 0;
  for (unsigned int i=0; i<n; ++i) {
    if (isFlow(i)==1) {
      flows(comptFlows) = i;
      comptFlows++;
    } else {
      stocks(comptStocks) = i;
      comptStocks++;
    }
  }

  const unsigned int qd = p - (nf==0);
  arma::mat bigF = zeros(n, n*p);
  arma::mat f;
  arma::mat covv = zeros(n*(qd + 1), n);

  carmaModel myCarmaModel(n, p, q, nf, h, boolInter, stocks, flows);
  myCarmaModel.cppEDRh(bFull, bigF, f, covv);
  return Rcpp::List::create(Rcpp::Named("bigF") = bigF,
                            Rcpp::Named("f") = f,
                            Rcpp::Named("covv") = covv);
}

// [[Rcpp::export]]
Rcpp::List EDRhforR(Rcpp::List bList) {
  const arma::vec bFull = bList["bFull"];
  const unsigned int n = bList["n"];
  const unsigned int  p = bList["p"];
  const unsigned int q = bList["q"];
  const double h = bList["h"];
  const bool boolInter = bList["boolInter"];
  arma::Col<unsigned int> isFlow = bList["isFlow"];
  return EDRhforR(bFull, n, p, q, h, boolInter, isFlow);
}

double LLKforR(const arma::vec bFull, // Computes the exact discrete time representation from Thornton and Chambers (2017), theorems 1 and 2.
			         arma::mat xTh,
               const unsigned int n,
               const unsigned int p,
               const unsigned int q,
               const double h,
               const bool boolInter,
               arma::Col<unsigned int> isFlow) {
  //Set stocks and flows
  unsigned int nf = accu(isFlow);
  unsigned int ns = n - nf;
  arma::Col<unsigned int> stocks(ns), flows(nf);
  unsigned int comptStocks = 0, comptFlows = 0;
  for (unsigned int i=0; i<n; ++i) {
    if (isFlow(i)==1) {
      flows(comptFlows) = i;
      comptFlows++;
    } else {
      stocks(comptStocks) = i;
      comptStocks++;
    }
  }

  const unsigned int qd = p - (nf==0);
  arma::mat bigF = zeros(n, n*p);
  arma::mat f;
  arma::mat covv = zeros(n*(qd + 1), n);
  carmaModel myCarmaModel(n, p, q, nf, h, boolInter, stocks, flows, xTh);
  return myCarmaModel.LLK(bFull);
}
// [[Rcpp::export]]
double LLKforR(Rcpp::List bList, arma::mat xTh) {
  const arma::vec bFull = bList["bFull"];
  const unsigned int n = bList["n"];
  const unsigned int p = bList["p"];
  const unsigned int q = bList["q"];
  const double h = bList["h"];
  const bool boolInter = bList["boolInter"];
  arma::Col<unsigned int> isFlow = bList["isFlow"];
  if(xTh.n_cols!=n) {
    Rcpp::Rcout<<"Error: xTh must have n columns, returning 0"<<std::endl;
    return 0;
  }
  
  return LLKforR(bFull, xTh, n, p, q, h, boolInter, isFlow);
}

// [[Rcpp::export]]
arma::mat createAugHForR(const arma::vec bFull, // Computes the exact discrete time representation from Thornton and Chambers (2017), theorems 1 and 2.
                          const unsigned int n,
                          const unsigned int p,
                          const unsigned int q,
                          const double h,
                          const bool boolInter,
                          arma::Col<unsigned int> isFlow) {
  //Set stocks and flows
  unsigned int nf = accu(isFlow);
  unsigned int ns = n - nf;
  arma::Col<unsigned int> stocks(ns), flows(nf);
  unsigned int comptStocks = 0, comptFlows = 0;
  for (unsigned int i=0; i<n; ++i) {
    if (isFlow(i)==1) {
      flows(comptFlows) = i;
      comptFlows++;
    } else {
      stocks(comptStocks) = i;
      comptStocks++;
    }
  }

  carmaModel myCarmaModel(n, p, q, nf, h, boolInter, stocks, flows);
  return myCarmaModel.createAugH(bFull);
}

// [[Rcpp::export]]
arma::mat createAugThetaForR(const arma::vec bFull, // Computes the exact discrete time representation from Thornton and Chambers (2017), theorems 1 and 2.
                             const unsigned int n,
                             const unsigned int p,
                             const unsigned int q,
                             const double h,
                             const bool boolInter,
                             arma::Col<unsigned int> isFlow) {
  //Set stocks and flows
  unsigned int nf = accu(isFlow);
  unsigned int ns = n - nf;
  arma::Col<unsigned int> stocks(ns), flows(nf);
  unsigned int comptStocks = 0, comptFlows = 0;
  for (unsigned int i=0; i<n; ++i) {
    if (isFlow(i)==1) {
      flows(comptFlows) = i;
      comptFlows++;
    } else {
      stocks(comptStocks) = i;
      comptStocks++;
    }
  }
  
  carmaModel myCarmaModel(n, p, q, nf, h, boolInter, stocks, flows);
  return myCarmaModel.createAugTheta(bFull);
}

// [[Rcpp::export]]
arma::vec createAugaForR(const arma::vec bFull, // Computes the exact discrete time representation from Thornton and Chambers (2017), theorems 1 and 2.
                         const unsigned int n,
                         const unsigned int p,
                         const unsigned int q,
                         const double h,
                         const bool boolInter,
                         arma::Col<unsigned int> isFlow) {
  //Set stocks and flows
  unsigned int nf = accu(isFlow);
  unsigned int ns = n - nf;
  arma::Col<unsigned int> stocks(ns), flows(nf);
  unsigned int comptStocks = 0, comptFlows = 0;
  for (unsigned int i=0; i<n; ++i) {
    if (isFlow(i)==1) {
      flows(comptFlows) = i;
      comptFlows++;
    } else {
      stocks(comptStocks) = i;
      comptStocks++;
    }
  }
  
  carmaModel myCarmaModel(n, p, q, nf, h, boolInter, stocks, flows);
  return myCarmaModel.createAuga(bFull);
}

// [[Rcpp::export]]
arma::mat createSigmaForR(const arma::vec bFull, // Computes the exact discrete time representation from Thornton and Chambers (2017), theorems 1 and 2.
                         const unsigned int n,
                         const unsigned int p,
                         const unsigned int q,
                         const double h,
                         const bool boolInter,
                         arma::Col<unsigned int> isFlow) {
  //Set stocks and flows
  unsigned int nf = accu(isFlow);
  unsigned int ns = n - nf;
  arma::Col<unsigned int> stocks(ns), flows(nf);
  unsigned int comptStocks = 0, comptFlows = 0;
  for (unsigned int i=0; i<n; ++i) {
    if (isFlow(i)==1) {
      flows(comptFlows) = i;
      comptFlows++;
    } else {
      stocks(comptStocks) = i;
      comptStocks++;
    }
  }
  
  carmaModel myCarmaModel(n, p, q, nf, h, boolInter, stocks, flows);
  return myCarmaModel.createSigma(bFull);
}

// [[Rcpp::export]]
Rcpp::List BFGSoptimForR(Rcpp::List bList,
                         arma::mat xTh,
                         const double numericalGradientStep,
                         size_t numBasis,
                         size_t maxIterations,
                         double armijoConstant,
                         double wolfe,
                         double minGradientNorm,
                         double factr,
                         size_t maxLineSearchTrials,
                         double minStep,
                         double maxStep) {
  //extract elements from bList
  arma::vec bFull = bList["bFull"];
  arma::Col<unsigned int> bFixed = bList["bFixed"];
  const arma::vec bLower = bList["bLower"];
  const arma::vec bUpper = bList["bUpper"];
  unsigned int n = bList["n"];
  unsigned int p = bList["p"];
  unsigned int q = bList["q"];
  double h = bList["h"];
  bool boolInter = bList["boolInter"];
  arma::Col<unsigned int> isFlow = bList["isFlow"];
  
  if(xTh.n_cols!=n) {
    Rcpp::Rcout<<"Error: xTh must have n columns, returning empty list"<<std::endl;
    return Rcpp::List::create();
  }
  
  //Set stocks and flows
  unsigned int nf = accu(isFlow);
  unsigned int ns = n - nf;
  arma::Col<unsigned int> stocks(ns), flows(nf);
  unsigned int comptStocks = 0, comptFlows = 0;
  for (unsigned int i=0; i<n; ++i) {
    if (isFlow(i)==1) {
      flows(comptFlows) = i;
      comptFlows++;
    } else {
      stocks(comptStocks) = i;
      comptStocks++;
    }
  }
  
  // Construct the objective function.
  arma::vec bFullInit = bFull;
  carmaModel myCarmaModel(n, p, q, nf, h, boolInter, stocks, flows, xTh);
  carmaFixed<carmaModel> myCarmaFixed(&myCarmaModel, bFixed, bFull, bLower, bUpper);
  BFGS<carmaFixed<carmaModel>> myBFGS(myCarmaFixed,
                    				          numericalGradientStep,
                    				          numBasis, // same default as scipy 
                    				          maxIterations, // many but not infinite 
                    				          armijoConstant,
                    				          wolfe,
                    				          minGradientNorm,
                    				          factr,
                    				          maxLineSearchTrials,
                    				          minStep,
                    				          maxStep);
  arma::vec bInit = myCarmaFixed.bPartGet(bFull);
  double LLK = myBFGS.Optimize(bInit);
  bFull = myCarmaFixed.bFullEdit(bInit);
  Rcpp::List bListSol = Rcpp::List::create(
    Rcpp::Named("n") = n,
    Rcpp::Named("p") = p,
    Rcpp::Named("q") = q,
    Rcpp::Named("h") = h,
    Rcpp::Named("boolInter") = boolInter,
    Rcpp::Named("isFlow") = isFlow,
    Rcpp::Named("bFull") = bFull,
    Rcpp::Named("bFixed") = bFixed,
    Rcpp::Named("bLower") = bLower,
    Rcpp::Named("bUpper") = bUpper);
  
  if(xTh.n_cols!=n) {
    Rcpp::Rcout<<"Error: xTh must have n columns, returning empty list"<<std::endl;
    return Rcpp::List::create();
  }
  
  if (LLK>=1e50) {
    Rcpp::Rcout<<"Failed to compute LLK, returning initial value"<<std::endl;
    return Rcpp::List::create(Rcpp::Named("bListSol") = bList,
                              Rcpp::Named("value") = LLK);
  }
  return Rcpp::List::create(Rcpp::Named("bListSol") = bListSol,
                            Rcpp::Named("value") = LLK);
}


// [[Rcpp::export]]
Rcpp::List CMAESoptimForR(Rcpp::List bList,
                          arma::mat xTh,
                          const unsigned int lambda, 
                          double sigma, 
                          const unsigned int nIterMax, 
                          const double tol) {
  //extract elements from bList
  arma::vec bFull = bList["bFull"];
  arma::Col<unsigned int> bFixed = bList["bFixed"];
  const arma::vec bLower = bList["bLower"];
  const arma::vec bUpper = bList["bUpper"];
  unsigned int n = bList["n"];
  unsigned int p = bList["p"];
  unsigned int q = bList["q"];
  double h = bList["h"];
  bool boolInter = bList["boolInter"];
  arma::Col<unsigned int> isFlow = bList["isFlow"];
  
  if(xTh.n_cols!=n) {
    Rcpp::Rcout<<"Error: xTh must have n columns, returning empty list"<<std::endl;
    return Rcpp::List::create();
  }
  
  //Set stocks and flows
  unsigned int nf = accu(isFlow);
  unsigned int ns = n - nf;
  arma::Col<unsigned int> stocks(ns), flows(nf);
  unsigned int comptStocks = 0, comptFlows = 0;
  for (unsigned int i=0; i<n; ++i) {
    if (isFlow(i)==1) {
      flows(comptFlows) = i;
      comptFlows++;
    } else {
      stocks(comptStocks) = i;
      comptStocks++;
    }
  }
  
  // Construct the objective function.
  const bool useParallel = true;
  arma::vec bFullInit = bFull;
  carmaModel myCarmaModel(n, p, q, nf, h, boolInter, stocks, flows, xTh);
  carmaFixed<carmaModel> myCarmaFixed(&myCarmaModel, bFixed, bFull, bLower, bUpper);
  CMAES<carmaFixed<carmaModel>> myCMAES(myCarmaFixed, lambda, sigma, nIterMax, tol, useParallel);
  arma::vec bInit = myCarmaFixed.bPartGet(bFull);
  double LLK = myCMAES.Optimize(bInit);
  bFull = myCarmaFixed.bFullEdit(bInit);
  Rcpp::List bListSol = Rcpp::List::create(
    Rcpp::Named("n") = n,
    Rcpp::Named("p") = p,
    Rcpp::Named("q") = q,
    Rcpp::Named("h") = h,
    Rcpp::Named("boolInter") = boolInter,
    Rcpp::Named("isFlow") = isFlow,
    Rcpp::Named("bFull") = bFull,
    Rcpp::Named("bFixed") = bFixed,
    Rcpp::Named("bLower") = bLower,
    Rcpp::Named("bUpper") = bUpper);
  
  if (LLK>=1e50) {
    Rcpp::Rcout<<"Failed to compute LLK, returning initial value"<<std::endl;
    return Rcpp::List::create(Rcpp::Named("bListSol") = bList,
                              Rcpp::Named("value") = LLK);
}
  return Rcpp::List::create(Rcpp::Named("bListSol") = bListSol,
                            Rcpp::Named("value") = LLK);
}

//[[Rcpp::export]]
Rcpp::List selectParametersBFGS(
    Rcpp::List bList,
    arma::mat& xTh,
    const std::string criterionType,
    const double numericalGradientStep,
    size_t numBasis,
    size_t maxIterations,
    double armijoConstant,
    double wolfe,
    double minGradientNorm,
    double factr,
    size_t maxLineSearchTrials,
    double minStep,
    double maxStep, 
    const size_t populationSize,
    const size_t maxGenerations,
    const double mutationProb,
    const double selectPercent,
    const double tolerance) {
  //extract elements from bList
  arma::vec bFull = bList["bFull"];
  arma::Col<unsigned int> bFixed = bList["bFixed"];
  arma::Col<unsigned int> bFixable = bList["bFixable"];
  const arma::vec bLower = bList["bLower"];
  const arma::vec bUpper = bList["bUpper"];
  const arma::vec bFullFixed = bList["bFullFixed"];
  unsigned int n = bList["n"];
  unsigned int p = bList["p"];
  unsigned int q = bList["q"];
  double h = bList["h"];
  bool boolInter = bList["boolInter"];
  arma::Col<unsigned int> isFlow = bList["isFlow"];
  
  if(xTh.n_cols!=n) {
    Rcpp::Rcout<<"Error: xTh must have n columns, returning empty list"<<std::endl;
    return Rcpp::List::create();
  }
  
  //Set stocks and flows
  unsigned int nf = accu(isFlow);
  unsigned int ns = n - nf;
  arma::Col<unsigned int> stocks(ns), flows(nf);
  unsigned int comptStocks = 0, comptFlows = 0;
  for (unsigned int i=0; i<n; ++i) {
    if (isFlow(i)==1) {
      flows(comptFlows) = i;
      comptFlows++;
    } else {
      stocks(comptStocks) = i;
      comptStocks++;
    }
  }
  
  const bool useParallel = false; // tells CMAES not to run in parallel because GA is already parallel
  // Init objects for optimization
  arma::Col<unsigned int> bFixedInit = bFixed;
  carmaModel myCarmaModel(n, p, q, nf, h, boolInter, stocks, flows, xTh);
  carmaFixed<carmaModel> myCarmaFixed(&myCarmaModel, bFixed, bFull, bLower, bUpper);
  BFGS<carmaFixed<carmaModel>> myBFGS(myCarmaFixed, numericalGradientStep,
                                      numBasis, maxIterations, armijoConstant, wolfe, minGradientNorm,
                                      factr, maxLineSearchTrials, minStep, maxStep);
  optimFixable<carmaModel, carmaFixed<carmaModel>, BFGS<carmaFixed<carmaModel>>> myOptimFixable(myBFGS,
                                                                                                 &myCarmaModel, bFixable,
                                                                                                 bFull, bFullFixed, 
                                                                                                 bFixedInit, bLower, bUpper,
                                                                                                 criterionType);
  GA<optimFixable<carmaModel, carmaFixed<carmaModel>, BFGS<carmaFixed<carmaModel>>>> myGA(myOptimFixable,
                                                                                           populationSize,
                                                                                           maxGenerations,
                                                                                           mutationProb,
                                                                                           selectPercent,
                                                                                           tolerance);
  arma::Col<unsigned int> bFixedPart(sum(bFixable));
  bFixedPart.fill(0);
  double crit = myGA.Optimize(bFixedPart);
  arma::Col<unsigned int> bFixedSol = myOptimFixable.getBFixedFull(bFixedPart);
  // RETURN RESULTS (a bit ugly but it works, problem here is that we only have the vector of selected parameters, but not the value of those parameters)
  carmaFixed<carmaModel> myCarmaFixedEnd(&myCarmaModel, bFixedSol, bFull, bLower, bUpper);
  BFGS<carmaFixed<carmaModel>> myBFGSEnd(myCarmaFixedEnd, numericalGradientStep,
                                         numBasis, maxIterations, armijoConstant, wolfe, minGradientNorm,
                                         factr, maxLineSearchTrials, minStep, maxStep);
  arma::vec bInitEnd = myCarmaFixedEnd.bPartGet(bFull);
  double LLK = myBFGSEnd.Optimize(bInitEnd);
  arma::vec bFullEnd = myCarmaFixedEnd.bFullEdit(bInitEnd);
  
  Rcpp::List bListSol = Rcpp::List::create(
    Rcpp::Named("n") = n,
    Rcpp::Named("p") = p,
    Rcpp::Named("q") = q,
    Rcpp::Named("h") = h,
    Rcpp::Named("boolInter") = boolInter,
    Rcpp::Named("isFlow") = isFlow,
    Rcpp::Named("bFull") = bFullEnd,
    Rcpp::Named("bFixed") = bFixedSol,
    Rcpp::Named("bFixable") = bFixable,
    Rcpp::Named("bLower") = bLower,
    Rcpp::Named("bUpper") = bUpper,
    Rcpp::Named("bFullFixed") = bFullFixed);
  return Rcpp::List::create(Rcpp::Named("criterion") = crit,
                            Rcpp::Named("LLK") = LLK,
                            Rcpp::Named("bList") = bListSol);
}

//[[Rcpp::export]]
Rcpp::List selectParametersCMAES(
              Rcpp::List bList,
							arma::mat& xTh,
							const std::string criterionType,
            	const unsigned int lambda, 
            	double sigma,
            	const unsigned int nIterMax, 
            	const double tol,
							const size_t populationSize,
							const size_t maxGenerations,
							const double mutationProb,
							const double selectPercent,
							const double tolerance) {
  //extract elements from bList
  arma::vec bFull = bList["bFull"];
  arma::Col<unsigned int> bFixed = bList["bFixed"];
  arma::Col<unsigned int> bFixable = bList["bFixable"];
  const arma::vec bLower = bList["bLower"];
  const arma::vec bUpper = bList["bUpper"];
  const arma::vec bFullFixed = bList["bFullFixed"];
  unsigned int n = bList["n"];
  unsigned int p = bList["p"];
  unsigned int q = bList["q"];
  double h = bList["h"];
  bool boolInter = bList["boolInter"];
  arma::Col<unsigned int> isFlow = bList["isFlow"];
  
  //Set stocks and flows
  unsigned int nf = accu(isFlow);
  unsigned int ns = n - nf;
  arma::Col<unsigned int> stocks(ns), flows(nf);
  unsigned int comptStocks = 0, comptFlows = 0;
  for (unsigned int i=0; i<n; ++i) {
    if (isFlow(i)==1) {
      flows(comptFlows) = i;
      comptFlows++;
    } else {
      stocks(comptStocks) = i;
      comptStocks++;
    }
  }

	const bool useParallel = false;

	// Init objects for optimization
	arma::Col<unsigned int> bFixedInit = bFixed;
	carmaModel myCarmaModel(n, p, q, nf, h, boolInter, stocks, flows, xTh);
	carmaFixed<carmaModel> myCarmaFixed(&myCarmaModel, bFixed, bFull, bLower, bUpper);
	CMAES<carmaFixed<carmaModel>> myCMAES(myCarmaFixed, lambda, sigma, nIterMax, tol, useParallel);
	optimFixable<carmaModel, carmaFixed<carmaModel>, CMAES<carmaFixed<carmaModel>>> myOptimFixable(myCMAES,
                                                                                                &myCarmaModel, bFixable,
                                                                                                bFull, bFullFixed, 
                                                                                                bFixedInit, bLower, bUpper,
                                                                                                criterionType);
	GA<optimFixable<carmaModel, carmaFixed<carmaModel>, CMAES<carmaFixed<carmaModel>>>> myGA(myOptimFixable,
                                                                                           populationSize,
                                                                            							 maxGenerations,
                                                                            							 mutationProb,
                                                                            							 selectPercent,
                                                                            							 tolerance);
	arma::Col<unsigned int> bFixedPart(sum(bFixable));
	bFixedPart.fill(0);
	double crit = myGA.Optimize(bFixedPart);
	Rcpp::Rcout<<crit<<std::endl;
	arma::Col<unsigned int> bFixedSol = myOptimFixable.getBFixedFull(bFixedPart);
	// RETURN RESULTS (a bit ugly but it works, problem here is that we only have the vector of selected parameters, but not the value of those parameters)
	carmaFixed<carmaModel> myCarmaFixedEnd(&myCarmaModel, bFixedSol, bFull, bLower, bUpper);
	CMAES<carmaFixed<carmaModel>> myCMAESEnd(myCarmaFixedEnd, lambda, sigma, nIterMax, tol, useParallel);
	arma::vec bInitEnd = myCarmaFixedEnd.bPartGet(bFull);
	double LLK = myCMAESEnd.Optimize(bInitEnd);
	arma::vec bFullEnd = myCarmaFixedEnd.bFullEdit(bInitEnd);
	
	Rcpp::List bListSol = Rcpp::List::create(
	  Rcpp::Named("n") = n,
	  Rcpp::Named("p") = p,
	  Rcpp::Named("q") = q,
	  Rcpp::Named("h") = h,
	  Rcpp::Named("boolInter") = boolInter,
	  Rcpp::Named("isFlow") = isFlow,
	  Rcpp::Named("bFull") = bFullEnd,
	  Rcpp::Named("bFixed") = bFixedSol,
	  Rcpp::Named("bFixable") = bFixable,
	  Rcpp::Named("bLower") = bLower,
	  Rcpp::Named("bUpper") = bUpper,
	  Rcpp::Named("bFullFixed") = bFullFixed);
	return Rcpp::List::create(Rcpp::Named("criterion") = crit,
                           Rcpp::Named("LLK") = LLK,
                            Rcpp::Named("bList") = bListSol);
	}
