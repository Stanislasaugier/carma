# ifndef BFGS_H_DEFINED
# define BFGS_H_DEFINED
#define ARMA_DONT_PRINT_ERRORS
#include <RcppArmadillo.h>

//****************************/
/*  Optimization using BFGS  */
/*****************************/


template<typename T>
class BFGS
{
public:
  // Default Constructor
  BFGS(T modelIn,
       const double numericalGradientStepIn = 1e-3,
       const size_t numBasisIn = 5, // same default as scipy
       const size_t maxIterationsIn = 10000, // many but not infinite
       const double armijoConstantIn = 1e-4,
       const double wolfeIn = 0.9,
       const double minGradientNormIn = 1e-6,
       const double factrIn = 1e-15,
       const size_t maxLineSearchTrialsIn = 100,
       const double minStepIn = 1e-20,
       const double maxStepIn = 1e20);
/*
  // Copy Constructor
  BFGS(const BFGS& BFGSin);
  //Destructor
  ~BFGS();
*/
  // Numerically Compute the Gradient
  arma::vec Gradient(arma::vec b);

  double ChooseScalingFactor(const size_t iterationNum, arma::vec& gradient,
                             const arma::mat& s, const arma::mat& y);

  /**
   * Find the L_BFGS search direction.
   *
   * @param gradient The gradient at the current point.
   * @param iterationNum The iteration number.
   * @param scalingFactor Scaling factor to use (see ChooseScalingFactor_()).
   * @param s Differences between the b and old b matrix.
   * @param y Differences between the gradient and the old gradient matrix.
   * @param searchDirection Vector to store search direction in.
   */
  void SearchDirection(const arma::vec& gradient, const size_t iterationNum,
                       const double scalingFactor, const arma::mat& s,
                       const arma::mat& y, arma::vec& searchDirection);

  /**
   * Update the y and s matrices, which store the differences between
   * the b and old b and the differences between the gradient and the
   * old gradient, respectively.
   *
   * @param iterationNum Iteration number.
   * @param b Current point.
   * @param oldb Point at last iteration.
   * @param gradient Gradient at current point (b).
   * @param oldGradient Gradient at last iteration point (oldb).
   * @param s Differences between the b and old b matrix.
   * @param y Differences between the gradient and the old gradient matrix.
   */
  void UpdateBasisSet(const size_t iterationNum, const arma::vec& b,
                      const arma::vec& oldb, const arma::vec& gradient,
                      const arma::vec& oldGradient, arma::mat& s, arma::mat& y);

  /**
   * Perform a back-tracking line search along the search direction to calculate a
   * step size satisfying the Wolfe conditions.
   *
   * @return false if no step size is suitable, true otherwise.
   */
  bool LineSearch(double& functionValue, arma::vec& b,
                  arma::vec& gradient, arma::vec& newbTmp,
                  const arma::vec& searchDirection, double& finalStepSize);
  // 
  double Optimize(arma::vec& b);
  
  void editModel(T& newModel);
  
private:
  // the object to optimize (class containing data and parameters defining the model's structure, i.e everything required for optimization)
  T model;
  // Steps for numerical gradient estimation
  const double numericalGradientStep;
  const size_t numBasis;
  //! Maximum number of iterations.
  const size_t maxIterations;
  //! Parameter for determining the Armijo condition.
  const double armijoConstant;
  //! Parameter for detecting the Wolfe condition.
  const double wolfe;
  //! Minimum gradient norm required to continue the optimization.
  const double minGradientNorm;
  //! Minimum relative function value decrease to continue the optimization.
  const double factr;
  //! Maximum number of trials for the line search.
  const size_t maxLineSearchTrials;
  //! Minimum step of the line search.
  const double minStep;
  //! Maximum step of the line search.
  const double maxStep;
};

#include "BFGS.tpp"
#endif
