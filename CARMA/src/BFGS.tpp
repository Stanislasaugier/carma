#define ARMA_DONT_PRINT_ERRORS
#include <RcppArmadillo.h>

#include "BFGS.h"
#include "carmaClass.h"
#include "carmaClassFixed.h"

//****************************/
/*  Optimization using BFGS  */
/*****************************/

template<typename T>
BFGS<T>::BFGS(T modelIn,
              const double numericalGradientStepIn,
              const size_t numBasisIn, /* same default as scipy */
              const size_t maxIterationsIn, /* many but not infinite */
              const double armijoConstantIn,
              const double wolfeIn,
              const double minGradientNormIn,
              const double factrIn,
              const size_t maxLineSearchTrialsIn,
              const double minStepIn,
              const double maxStepIn) :
	model(modelIn),
	numericalGradientStep(numericalGradientStepIn), 
	numBasis(numBasisIn),  maxIterations(maxIterationsIn), armijoConstant(armijoConstantIn),
	wolfe(wolfeIn), minGradientNorm(minGradientNormIn), factr(factrIn),
	maxLineSearchTrials(maxLineSearchTrialsIn), minStep(minStepIn),
	maxStep(maxStepIn) { }
/*
template<typename T>
BFGS<T>::BFGS(const BFGS<T>& BFGSin) :
    bInit(BFGSin.bInit),
	numericalGradientStep(BFGSin.numericalGradientStep), 
	numBasis(BFGSin.numBasis),  maxIterations(BFGSin.maxIterations), armijoConstant(BFGSin.armijoConstant),
	wolfe(BFGSin.wolfe), minGradientNorm(BFGSin.minGradientNorm), factr(BFGSin.factr),
	maxLineSearchTrials(BFGSin.maxLineSearchTrials), minStep(BFGSin.minStep), maxStep(BFGSin.maxStep)
 {
	model = new T(*BFGSin.model);
}

template<typename T>
BFGS<T>::~BFGS() {
		delete model;
}
*/	

template<typename T>
arma::vec BFGS<T>::Gradient(arma::vec b) {
  arma::vec out(b.n_elem);
  try {
    double init = model.Evaluate(b);
    for (unsigned int i=0; i<b.n_elem; i++) {
      b[i] += numericalGradientStep;
      out[i] = (model.Evaluate(b) - init)/numericalGradientStep;
      b[i] -= numericalGradientStep;
    }
  } catch(...) {
    for (unsigned int i =0; i<b.n_elem; i++) {
      out[i] = 1;
      // Rcpp::Rcout<<"WARNING: Gradient Failed"<<std::endl;
    }
  }
  return out;
}
template<typename T>
double BFGS<T>::ChooseScalingFactor(const size_t iterationNum,
                                 arma::vec& gradient,
                                 const arma::mat& s,
                                 const arma::mat& y)
{
  double scalingFactor = 1.0;
  if (iterationNum > 0)
  {
    int previousPos = (iterationNum - 1) % numBasis;
    // Get s and y matrices once instead of multiple times.
    const arma::Mat<double>& sMat = s.col(previousPos);
    const arma::Mat<double>& yMat = y.col(previousPos);
    scalingFactor = dot(sMat, yMat) / dot(yMat, yMat);
  }
  else
  {
    scalingFactor = 1.0 / sqrt(dot(gradient, gradient));
  }

  return scalingFactor;
}

/**
 * Find the L_BFGS search direction.
 *
 * @param gradient The gradient at the current point.
 * @param iterationNum The iteration number.
 * @param scalingFactor Scaling factor to use (see ChooseScalingFactor_()).
 * @param s Differences between the b and old b matrix.
 * @param y Differences between the gradient and the old gradient matrix.
 * @param searchDirection Vector to store search direction in.
 */
template<typename T>
void BFGS<T>::SearchDirection(const arma::vec& gradient,
                           const size_t iterationNum,
                           const double scalingFactor,
                           const arma::mat& s,
                           const arma::mat& y,
                           arma::vec& searchDirection)
{
  // Start from this point.
  searchDirection = gradient;

  // See "A Recursive Formula to Compute H * g" in "Updating quasi-Newton
  // matrices with limited storage" (Nocedal, 1980).
  // typedef typename CubeType::elem_type CubeElemType;

  // Temporary variables.
  arma::Col<double> rho(numBasis);
  arma::Col<double> alpha(numBasis);

  size_t limit = (numBasis > iterationNum) ? 0 : (iterationNum - numBasis);
  for (size_t i = iterationNum; i != limit; i--)
  {
    int translatedPosition = (i + (numBasis - 1)) % numBasis;
    rho[iterationNum - i] = 1.0 / arma::dot(y.col(translatedPosition),
                                            s.col(translatedPosition));
    alpha[iterationNum - i] = rho[iterationNum - i] *
      arma::dot(s.col(translatedPosition), searchDirection);
    searchDirection -= alpha[iterationNum - i] * y.col(translatedPosition);
  }

  searchDirection *= scalingFactor;

  for (size_t i = limit; i < iterationNum; i++)
  {
    int translatedPosition = i % numBasis;
    double beta = rho[iterationNum - i - 1] *
      arma::dot(y.col(translatedPosition), searchDirection);
    searchDirection += (alpha[iterationNum - i - 1] - beta) *
      s.col(translatedPosition);
  }

  // Negate the search direction so that it is a descent direction.
  searchDirection *= -1;
}

/**
 * Update the y and s matrices, which store the differences between
 * the b and old b and the differences between the gradient and the
 * old gradient, respectively.
 *
 * @param iterationNum Iteration number.
 * @param b Current point.
 * @param oldb Point at last iteration.
 * @param gradient Gradient at current point (b).
 * @param oldGradient Gradient at last iteration point (oldb).
 * @param s Differences between the b and old b matrix.
 * @param y Differences between the gradient and the old gradient matrix.
 */
template<typename T>
void BFGS<T>::UpdateBasisSet(const size_t iterationNum,
                          const arma::vec& b,
                          const arma::vec& oldb,
                          const arma::vec& gradient,
                          const arma::vec& oldGradient,
                          arma::mat& s,
                          arma::mat& y)
{
  // Overwrite a certain position instead of pushing everything in the vector
  // back one position.
  int overwritePos = iterationNum % numBasis;
  s.col(overwritePos) = b - oldb;
  y.col(overwritePos) = gradient - oldGradient;
}

/**
 * Perform a back-tracking line search along the search direction to calculate a
 * step size satisfying the Wolfe conditions.
 *
 * @return false if no step size is suitable, true otherwise.
 */
template<typename T>
bool BFGS<T>::LineSearch(double& functionValue,
                      arma::vec& b,
                      arma::vec& gradient,
                      arma::vec& newbTmp,
                      const arma::vec& searchDirection,
                      double& finalStepSize)
{
  // Default first step size of 1.0.
  double stepSize = 1.0;
  finalStepSize = 0.0; // Set only when we take the step.

  // The initial linear term approximation in the direction of the
  // search direction.
  double initialSearchDirectionDotGradient =
    arma::dot(gradient, searchDirection);

  // If it is not a descent direction, just report failure.
  if (initialSearchDirectionDotGradient > 1e-5)
  {
    // Rcpp::Rcout << "L-BFGS line search direction "<< initialSearchDirectionDotGradient << " is not a descent direction "
    // << "(terminating)!" << std::endl;
    // return false;
  }

  // Save the initial function value.
  double initialFunctionValue = functionValue;

  // Unit linear approximation to the decrease in function value.
  double linearApproxFunctionValueDecrease = armijoConstant *
    initialSearchDirectionDotGradient;

  // The number of iteration in the search.
  size_t numIterations = 0;

  // Armijo step size scaling factor for increase and decrease.
  const double inc = 2.1;
  const double dec = 0.5;
  double width = 0;
  double bestStepSize = 1.0;
  double bestObjective = std::numeric_limits<double>::max();

  while (true)
  {
    // Perform a step and evaluate the gradient and the function values at that
    // point.
    newbTmp = b;
    newbTmp += stepSize * searchDirection;
    functionValue = model.Evaluate(newbTmp);
    gradient = Gradient(newbTmp);
    // terminate |= Callback::EvaluateWithGradient(*this, function, newbTmp,
    //                                             functionValue, gradient, callbacks...);

    if (functionValue < bestObjective)
    {
      bestStepSize = stepSize;
      bestObjective = functionValue;
    }
    numIterations++;

    if (functionValue > initialFunctionValue + stepSize *
        linearApproxFunctionValueDecrease)
    {
      width = dec;
    }
    else
    {
      // Check Wolfe's condition.
      double searchDirectionDotGradient = arma::dot(gradient,
                                                    searchDirection);

      if (searchDirectionDotGradient < wolfe *
          initialSearchDirectionDotGradient)
      {
        width = inc;
      }
      else
      {
        if (searchDirectionDotGradient > -wolfe *
            initialSearchDirectionDotGradient)
        {
          width = dec;
        }
        else
        {
          break;
        }
      }
    }

    // Terminate when the step size gets too small or too big or it
    // exceeds the max number of iterations.
    const bool cond1 = (stepSize < minStep);
    const bool cond2 = (stepSize > maxStep);
    const bool cond3 = (numIterations >= maxLineSearchTrials);
    if (cond1 || cond2 || cond3)
      break;

    // Scale the step size.
    stepSize *= width;
  }

  // Move to the new b.
  b += bestStepSize * searchDirection;
  finalStepSize = bestStepSize;
  return true;
}

template<typename T>
double BFGS<T>::Optimize(arma::vec& b)
{
  arma::vec bInit = b;
  const unsigned int nEltsb = b.n_elem;
  // Ensure that the cubes holding past iterations information are the right
  // size.  Also set the current best point value to the maximum.
  const size_t rows = nEltsb;
  // const size_t cols = b.n_cols;

  arma::vec newbTmp(rows);
  arma::mat s(rows, numBasis);
  arma::mat y(rows, numBasis);

  // The old b to be saved.
  arma::vec oldb(b.n_elem);
  oldb.zeros();

  // Whether to optimize until convergence.
  bool optimizeUntilConvergence = (maxIterations == 0);

  // The gradient: the current and the old.
  arma::vec gradient(b.n_elem);
  gradient.zeros();
  arma::vec oldGradient(b.n_elem);
  oldGradient.zeros();
  // The search direction.
  arma::vec searchDirection(b.n_elem);
  searchDirection.zeros();
  // The initial function value and gradient.
  double functionValue = model.Evaluate(b);
  gradient = Gradient(b);
  // terminate |= Callback::EvaluateWithGradient(*this, f, b,
  //       functionValue, gradient, callbacks...);
  double prevFunctionValue = model.Evaluate(b);
  oldGradient = Gradient(b);
  // The main optimization loop.
  // terminate |= Callback::BeginOptimization(*this, f, b, callbacks...);

  bool terminate = false;
  int itNumOut = 0;
  for (size_t itNum = 0; (optimizeUntilConvergence || (itNum != maxIterations))
    && !terminate; ++itNum)
  {
	itNumOut++;
    prevFunctionValue = functionValue;
    // Rcpp::Rcout<<b.t()<<std::endl;
    // Break when the norm of the gradient becomes too small.
    // But dont do this on the first iteration to ensure we always take at
    // least one descent step.
    if (itNum > 0 && (arma::norm(gradient, 2) < minGradientNorm))
    {
       //Rcpp::Rcout << "L-BFGS gradient norm too small (terminating successfully) at iteration: "<<itNum
       //<< "\n";
      break;
    }

    // Break if the objective is not a number.
    if (std::isnan(functionValue))
    {
       //Rcpp::Rcout << "L-BFGS terminated with objective " << functionValue << "; "
       //<< "are the objective and gradient functions implemented correctly?"
       //<< "\n";
      break;
    }

    // Choose the scaling factor.
    double scalingFactor = ChooseScalingFactor(itNum, gradient, s, y);
    // Build an approximation to the Hessian and choose the search
    // direction for the current iteration.
    SearchDirection(gradient, itNum, scalingFactor, s, y, searchDirection);
    // Save the old b and the gradient before stepping.
    oldb = b;
    oldGradient = gradient;

    double stepSize; // Set by LineSearch().
    if (!LineSearch(functionValue, b, gradient, newbTmp,
                    searchDirection, stepSize))
    {
      //Rcpp::Rcout << "Line search failed.  Stopping optimization." << "\n";
      break; // The line search failed; nothing else to try.
    }
    // It is possible that the difference between the two coordinates is zero.
    // In this case we terminate successfully.
    if (stepSize == 0.0)
    {
       //Rcpp::Rcout << "L-BFGS step size of 0 (terminating successfully) at iteration: "<<itNum
       //<< "\n";

      break;
    }
    // If we cant make progress on the gradient, then we ll also accept
    // a stable function value.
    const double denom = std::max(
      std::max(std::abs(prevFunctionValue), std::abs(functionValue)),
      (double) 1.0);
    if ((prevFunctionValue - functionValue) / denom <= factr)
    {
      // Rcpp::Rcout << "L-BFGS function value stable (terminating successfully). at iteration: "<<itNum
      // << "\n";
      // Rcpp::Rcout<<"Function value change in %: "<<(prevFunctionValue - functionValue) / denom<<std::endl;
      break;
    }
    // Overwrite an old basis set.
    UpdateBasisSet(itNum, b, oldb, gradient, oldGradient, s, y);
  } // End of the optimization loop.
//  Rcpp::Rcout<<"Terminating At Iteration: "<<itNumOut<<std::endl;
  bool bIsNan = false;
  for (unsigned int i = 0; i<b.n_elem; ++i) {
  	if (std::isnan(b[i])) {
		bIsNan = true;
	}
  }
  
  if (bIsNan) {
    b = bInit;
  }
  
  return functionValue;
}

template<typename T>
void BFGS<T>::editModel(T& newModel) {
    model = newModel;
}

