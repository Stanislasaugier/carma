# ifndef CARMAFIXEDLLK_H_DEFINED

# define CARMAFIXEDLLK_H_DEFINED
#define ARMA_DONT_PRINT_ERRORS
#include <RcppArmadillo.h>

template<typename T>
class carmaFixed {
public:

  // CONSTRUCTOR
  carmaFixed(T* aCarmaModelIn, 
             const arma::Col<unsigned int> bFixedIn, 
             const arma::vec bFullInitIn, 
             const arma::vec bLowerIn, 
             const arma::vec bUpperIn) :
      aCarmaModel(aCarmaModelIn), 
      bFixed(bFixedIn), 
      bFullInit(bFullInitIn), 
      nEltsBFull(bFullInitIn.n_elem), 
      bLower(bLowerIn), 
      bUpper(bUpperIn) { }
  
  carmaFixed& operator=(carmaFixed& carmaFixedIn) {
    this->aCarmaModel = carmaFixedIn.aCarmaModel; 
    this->bFixed = carmaFixedIn.bFixed; 
    this->bFullInit = carmaFixedIn.bFullInit; 
    this->nEltsBFull = carmaFixedIn.nEltsBFull; 
    this->bLower = carmaFixedIn.bLower; 
    this->bUpper = carmaFixedIn.bUpper; 
    return *this;
  }
  
  // Create vector bFull from class member bFullInit and input partial vector b 
  arma::vec bFullEdit(arma::vec b) {
    arma::vec bFull(nEltsBFull);
    unsigned int compt = 0;
    for (unsigned int it=0; it<nEltsBFull; it++) {
      if (bFixed[it]==1) {
        bFull[it] = b[compt];
        compt++;
      } else {
        bFull[it] = bFullInit[it];
      }
    }
    return bFull;
  }
  
  // Return the partial vector b of non fixed elements only
  arma::vec bPartGet(arma::vec bFull) {
    arma::vec b(sum(bFixed));
    unsigned int compt = 0;
    for (unsigned int i = 0; i<nEltsBFull; ++i) {
      if (bFixed(i)==1) {
        b(compt) = bFull(i);
        ++compt;
      }
    }
    return b;
  }
  
  
  // convert vector b of variable elements to a vector bFull of fixed elements and estimate (PENALIZED) LLK
  double Evaluate(arma::vec b) {
    arma::vec bFull = bFullEdit(b);
    double lmm = aCarmaModel->Evaluate(bFull);
    bool outOfBounds = false;
    for (unsigned int i=0; i<bFullInit.n_elem; ++i) { 
      if (bFull(i)>bUpper(i) || bFull(i)<bLower(i)) {
        outOfBounds = true;
      }
    }
    if (outOfBounds==true) {
      lmm+=aCarmaModel->penalize(bFull, bFullInit);
    }
    return lmm;
  }
 
private:
  T* aCarmaModel;
  arma::Col<unsigned int> bFixed;
  arma::vec bFullInit;
  unsigned int nEltsBFull;
  arma::vec bLower; 
  arma::vec bUpper;
};
#endif