#define ARMA_DONT_PRINT_ERRORS
#include <RcppArmadillo.h>

using namespace arma;

// Various fonctions to simulate a carma model

// [[Rcpp::export]]
arma::mat makeGaussianVector(unsigned int nElem,
                             arma::mat & Sigma,
                             arma::mat & posInit,
                             bool & usePosInit,
                             unsigned int & n,
                             unsigned int & p ) {
  arma::mat Y = randn(nElem, Sigma.n_cols);
  arma::mat cholMat = chol(symmatu(Sigma), "lower");
  if (usePosInit==TRUE) {
    for (unsigned int i=0; i<p; i++) {
      Y(0, span(i*n, (i+1)*n - 1)) = posInit.row(i)*cholMat(span(i*n, (i+1)*n - 1), span(i*n, (i+1)*n - 1)).i();
    }
  }
  return Y * cholMat;
}

// [[Rcpp::export]]
arma::mat makeVARMAcpp(unsigned int & nObs,
                       unsigned int & n,
                       unsigned int & p,
                       unsigned int & qdt,
                       bool & boolInter,
                       arma::mat & bigF,
                       arma::mat & covv,
                       arma::vec & f,
                       arma::mat & posInit,
                       bool & usePosInit) {
  arma::mat out = zeros(nObs, n);
  arma::mat covMat = zeros(n*nObs, n*nObs);
  arma::mat resInit = zeros(p, n);
  unsigned int i, j;
  
  for (i=0; i<nObs; i++) { // build the aggregate covariance matrix
    covMat.submat(n*i, n*i, n*(i+1) - 1, n*(i+1) - 1) = covv.rows(span(0, n-1));
    if (qdt>0 && i<nObs-1) {
      covMat.submat(n*(i+1), n*i, n*(i+2) - 1, n*(i+1) - 1) = covv.rows(span(n, 2*n-1));
      covMat.submat(n*i, n*(i+1), n*(i+1) - 1, n*(i+2) - 1) = covv.rows(span(n, 2*n-1)).t();
    }
    if (qdt>1 && i<nObs-2) {
      covMat.submat(n*(i+2), n*i, n*(i+3) - 1, n*(i+1) - 1) = covv.rows(span(2*n, 3*n-1));
      covMat.submat(n*i, n*(i+2), n*(i+1) - 1, n*(i+3) - 1) = covv.rows(span(2*n, 3*n-1)).t();
    }
  }
  
  arma::vec eps = vectorise(makeGaussianVector(1, covMat, resInit, usePosInit, n, p));
  if (usePosInit==TRUE) {
    Rcpp::Rcout<<out.row(i)<<std::endl;
    Rcpp::Rcout<<posInit.row(i)<<std::endl;
    for (i=0; i<p; i++) {
      out.row(i) = posInit.row(i);
    }
  } else {
    for (i=0; i<p; i++) {
      out.row(i) = eps(span(n*i, n*(i+1) - 1)).t();
    }
  }
  for (i=p; i<nObs; i++) {
    out.row(i) = f.t() + eps(span(n*i, n*(i+1) - 1)).t();
    for (j=0; j<p; j++) {
      out.row(i) += (bigF.cols(j*n, (j+1)*n - 1)*out.row(i-j-1).t()).t();
      
    }
  }
  return out;
}
