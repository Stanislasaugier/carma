#define ARMA_DONT_PRINT_ERRORS
#include <RcppArmadillo.h>
#include <ctime>
#include <math.h>

#include "carmaClass.h"

using namespace arma;

// CONSTRUCTOR
carmaModel::carmaModel(const unsigned int nIn,
                       const unsigned int pIn,
                       const unsigned int qIn,
                       const unsigned int nfIn,
                       const double hIn,
                       const bool boolInterIn,
                       const arma::Col<unsigned int> & stocksIn,
                       const arma::Col<unsigned int> & flowsIn,
                       const arma::mat & xThIn) : 
    n(nIn), p(pIn), q(qIn), nf(nfIn), h(hIn), boolInter(boolInterIn),
    stocks(stocksIn), flows(flowsIn), r(nIn*(pIn - 1) + nfIn), ns(nIn - nfIn),
    m(pIn - (nfIn==0)), Time(xThIn.n_rows - p) {
  S1 = createS1();
  S2 = createS2();
  x = dataBlockCpp(xThIn);
}

// Alternate Constructor, does not require any data as input
// Used to write discrete time representation of a continuous time model, which does not require any data.
carmaModel::carmaModel(const unsigned int nIn,
                       const unsigned int pIn,
                       const unsigned int qIn,
                       const unsigned int nfIn,
                       const double hIn,
                       const bool boolInterIn,
                       const arma::Col<unsigned int> & stocksIn,
                       const arma::Col<unsigned int> & flowsIn) : 
    n(nIn), p(pIn), q(qIn), nf(nfIn), h(hIn), boolInter(boolInterIn),
    stocks(stocksIn), flows(flowsIn), r(nIn*(pIn - 1) + nfIn), ns(nIn - nfIn),
    m(pIn - (nfIn==0)), Time(0) {
  S1 = createS1();
  S2 = createS2();
}

carmaModel::carmaModel(const carmaModel& carmaModelIn):
	n(carmaModelIn.n), p(carmaModelIn.p), q(carmaModelIn.q), 
	nf(carmaModelIn.nf), h(carmaModelIn.h), boolInter(carmaModelIn.boolInter),
	stocks(carmaModelIn.stocks), flows(carmaModelIn.flows),
	r(carmaModelIn.r), ns(carmaModelIn.ns),
	m(carmaModelIn.m), Time(carmaModelIn.Time),
  S1(carmaModelIn.S1), S2(carmaModelIn.S2), x(carmaModelIn.x) {}

// Create the matrix H from Thornton and Chambers, 2017, equation (8). Does not take the exponential of it  
arma::mat carmaModel::createAugH(const arma::vec bFull) {
  const unsigned int dimH = n*p + nf;
  arma::mat augH = zeros(dimH, dimH);
  
  if (nf>0) { // If there is a flow
    // augH.submat(0, n, nf - 1, n + nf - 1) = eye(nf, nf)/h;
    for (unsigned int i=0; i<nf; i++) {
      augH(i, nf + flows(i)) = 1/h;
    }
  }
  for (unsigned int i=0; i<n; i++) { // Add the column block of Ap-1, ..., A0
    augH(span(nf, dimH-1), (nf + i)) =
      bFull(span(p*n*i, p*n*(i+1)-1));
  }
  if (p>1) { // Add diagonal block if p>1
    for (unsigned int i = 0; i<(p-1); i++) {
      augH.submat(nf + n*i, nf + n*(i+1), nf + n*(1 + i) - 1, nf + n*(2 + i) - 1).eye();
    }
  }
  return augH;
}
 
 // Create the vector a from Thornton and Chambers, 2017, equation (8)
 arma::vec carmaModel::createAuga (const arma::vec bFull) {
   const unsigned int nAr = n*n*p;
   unsigned int dimAuga = n*p + nf;
   vec Auga = zeros(dimAuga);
   
   if (boolInter==0) {
     return Auga;
   }
   Auga(span(dimAuga - n, dimAuga - 1)) = bFull(span(nAr, nAr + n - 1));
   return Auga;
 }

// Create the matrix theta from Thornton and Chambers, 2017, equation (8)
arma::mat carmaModel::createAugTheta (const arma::vec bFull) {
  const unsigned int nAr = n*n*p;
  const unsigned int dimAugTheta = n*p +nf;
  arma::mat augTheta = zeros(dimAugTheta, n);
  if (q>0) {
    for (unsigned int i=0; i<n; i++) {
      augTheta(span(nf + (p - q - 1)*n, nf + (p - q - 1)*n + q*n - 1), i) = bFull(span(nAr + boolInter*n + n*q*i, nAr + boolInter*n + n*q*(i+1) - 1));
    }
  }
  augTheta.rows(dimAugTheta - n, dimAugTheta - 1) = eye(n, n);
  return augTheta;
}

//Creates the matrix sigma of the continous time model, makes sure that the resulting matrix is symmetric
arma::mat carmaModel::createSigma (const arma::vec bFull) {
  const unsigned int nAr = n*n*p;
  unsigned int compt = 0;
  arma::mat sigma = mat(n, n);
  
  for (unsigned int j=0; j<n; j++ ) {
    for (unsigned int i=j; i<n; i++) {
      sigma(i,j) = bFull(nAr + n*n*q + boolInter*n + compt);
      compt+=1;
      if (i!=j) {
        sigma(j, i) = sigma(i,j);
      }
    }
  }
  return sigma;
}
// Computes the exact discrete time representation from Thornton and Chambers (2017), theorems 1 and 2.
void carmaModel::cppEDRh(const arma::vec bFull, arma::mat& bigF, arma::mat& f, arma::mat& covv) {
//  std::clock_t start = std::clock();
  arma::mat Mhat, omega;
  const arma::mat H = createAugH(bFull)*h;
  const arma::vec a = createAuga(bFull)*h;
  const arma::mat theta = createAugTheta(bFull);
  const arma::mat sigma = createSigma(bFull)*h;
//  std::clock_t augH = std::clock();

  // Various intermediate variables used for expmat(H) and omegaExpMat
  arma::mat J = theta * sigma * theta.t();
  const unsigned int dimH = H.n_rows;
  const unsigned int N = 7; //constant for expmat
  const double normVal = std::max(arma::norm(H, "inf"), arma::norm(J, "inf"));
  const double logVal = (normVal > 0) ? double(std::log(normVal)) : double(0); // log because we'll take exponential of the matrix
  int exponent = 0;  std::frexp(logVal, &exponent);
  const arma::uword s = arma::uword( (std::max)(0, exponent + 5) );
//  arma::mat bigC2 = expmat(H);
  std::vector<arma::mat> allPowH(N, arma::mat(dimH, dimH)); //store exponents of H for exp(Mstar)
  std::vector<arma::mat> allPowExpAoverS(s+1, arma::mat(dimH, dimH)); //store exponents of H for exp(Mstar)
  allPowH[0] = H/pow(2, s);
  arma::mat bigC = myExpmatForH(J, dimH, s, allPowH, allPowExpAoverS);
//  std::clock_t expmat = std::clock();
  if (nf!=0) {
    bigC.submat(0,0, nf-1, nf-1).zeros();
  }
  if (S2.n_rows==1 && S2.n_cols==1 && S2(0,0)==0) { //Simple AR1 pure stock case
    bigF = bigC;
    f = phiMat(H)*a;
//    std::clock_t preOmegaEps = std::clock();
    covv = omegaEpsMat(J, dimH, s, allPowH, allPowExpAoverS);
//    std::clock_t endSimpleCase = std::clock();
//	Rcpp::Rcout<<"Simple AR1 pure stock case, computation time: init: "<<(augH-start)
//	<<" expmat: "<<(expmat-augH)<<" preOmegaEps: "<<(preOmegaEps - expmat)
//	<<" end "<<(endSimpleCase-preOmegaEps)<<std::endl;
    return;
  }
  else {
    arma::mat c11 = S1 * bigC * S1.t();
    arma::mat c12 = S1 * bigC * S2.t();
    arma::mat c21 = S2 * bigC * S1.t();
    arma::mat c22 = S2 * bigC * S2.t();
    arma::mat bigN = createN(c11, c21);
    arma::mat bigM = createM(c12, c22);
//    std::clock_t bigMandN = std::clock();
    if (nf==0 or nf==n) {
      Mhat = inv(bigM);
      Mhat = Mhat.rows(0, r-1);
    }
    else {
      // Rcpp::Rcout<<"I need T"<<std::endl;
      arma::mat bigT = createT(c11, c21);

      if (bigM.n_rows!=bigM.n_cols) {
        bigM = bigT * bigM;
      }
      else {
        //Rcpp::Rcout<<"No Need to multiply M by T???"<<std::endl;
      }
      Mhat = inv(bigM);
      Mhat = Mhat.rows(0, r-1) * bigT;
    }
//	std::clock_t makeMHat = std::clock();
    arma::mat CMhat = c12*Mhat;
    // Compute F
    bigF = mat(n, n*p);
    bigF.submat(0, 0, n-1, n-1) = c11 + CMhat*bigN.cols(0,n - 1);
    if (p>1) {
      for (unsigned int i=1; i<p; i++) {
        bigF.submat(0, n*i, n-1, n*(i+1)-1) = CMhat*bigN.cols(n*i,n*(i+1) - 1);
      }
    }
    
    // Compute f
    arma::vec c = phiMat(H)*a;
    arma::vec c1 = S1*c;
    arma::vec c2 = S2*c;
    arma::vec cBar = vec(m*(n+r));
    
    cBar(span(0, m*n - 1)) = repmat(c1, m, 1);
    cBar(span(m*n, m*(n+r) - 1)) = repmat(c2, m, 1);
    
    f = c1 + CMhat*cBar;
//    std::clock_t fAndBigF = std::clock();
    // Compute covv
//  omega = omegaEpsMat(H, theta, sigma);
    omega = omegaEpsMat(J, dimH, s, allPowH, allPowExpAoverS);
//    std::clock_t omegaEps = std::clock();
    arma::mat Ci = mat((m+1)*n, (n+r));
    Ci.submat(0, 0, n - 1, n+r - 1) = S1;
    for (unsigned int i=1; i<(m+1); i++) {
      Ci.submat(i*n, 0, (i+1)*n - 1, n+r - 1) = CMhat.submat(0, n*(i-1), n - 1, n*i - 1) * S1 + CMhat.submat(0,n*m + r*(i-1), n - 1, n*m + r*i - 1) * S2;
    }
    // arma::mat CiOmega = Ci * omega;
    for (unsigned int j=0; j<(m+1); j++) {
      for (unsigned int i=j; i<(m+1); i++) {
        covv.submat(n*j, 0, n*(j+1) - 1, n - 1) += Ci.submat(n*i, 0, (i+1)*n - 1, (n+r) - 1) * omega * Ci.submat(n*(i-j), 0, (i-j+1)*n - 1, (n+r) - 1).t();
      }
    }
/*	  std::clock_t end = std::clock();
	Rcpp::Rcout<<"computation time: init: "<<(augH-start)
	<<" expmat: "<<(expmat-augH)<<" bigMandN: "<<(bigMandN - expmat)
	<<" makeMHat "<<(makeMHat - bigMandN)
	<<" fandBigF"<<(fAndBigF - makeMHat)<<" omegaEps "<<omegaEps - fAndBigF
	<< " end "<<end - omegaEps<<std::endl;*/
  }
}

/***************************************************/
/********** COMPUTATION OF THE LIKELIHOOD **********/
/***************************************************/

// Computes the Choleski decomposition of a Toeplitz block matrix
void carmaModel::blockCholCpp(arma::mat & omega, unsigned int T, arma::mat& U, arma::mat& Uinv) {
  const unsigned int qd = p - (nf==0);
  unsigned int i, j;
  
  const double tol = 1e-10;
  arma::mat Urow(n, n*(qd+1));
  arma::mat dU = ones(n, n*(qd+1));
  
  U.submat(0, n*qd, n-1, (qd+1)*n-1) = chol(symmatu(omega.rows(0, n-1)), "lower");
  Uinv = inv(arma::trimatl(U.submat(0, n*qd, n-1, (qd+1)*n-1)));
  
  if (qd==0) {
    return;
  }
  for (i=1; i<qd; i++) {
    Urow.zeros();
    Urow.cols(n*(qd-i), n*(qd-i+1) - 1) = omega.rows(n*i, n*(i+1) - 1) * Uinv.rows(0, n - 1).t();
    if (i>1){
      for (j=1; j<i; j++) {
        Urow.cols(n*(qd-i+j), n*(qd-i+j+1) - 1) = (omega.rows(n*(i-j), n*(i-j+1) - 1) - Urow.cols(n*(qd-i), n*(qd-i+j) - 1) * U.submat(n*j, n*(qd-j), n*(j+1) - 1, n*qd - 1).t()) * Uinv.rows(n*j, n*(j+1) - 1).t();
      }
    }
    
    Urow.cols(n*qd, n*(qd+1) - 1) = chol(symmatu(omega.rows(0, n-1) - symmatu(Urow.cols(0, n*qd - 1) * Urow.cols(0, n*qd - 1).t())), "lower");
    U = join_cols(U, Urow);
    Uinv = join_cols(Uinv, inv(Urow.cols(n*qd, n*(qd+1) - 1)));
  }
  i = qd;
  while (max(max(dU))>tol && i<T) {
    Urow.zeros();
    Urow.cols(0, n - 1) = omega.rows(n*(qd), n*(qd+1) - 1) * Uinv.rows(n*(i - qd), n*(i - qd + 1) - 1).t();
    for (j = 1; j<qd; j++) {
      Urow.cols(n*j, n*(j+1) - 1) = ( omega.rows(n*(qd-j), n*(qd-j+1) - 1) - Urow.cols(0, n*j - 1) * U.submat(n*(i-qd+j),  n*(qd-j), n*(i-qd+j+1) - 1, n*(qd) - 1).t()) * Uinv.rows(n*(i-qd+j), n*(i-qd+j+1) - 1).t();
    }
    Urow.cols(n*qd, n*(qd+1) - 1) = chol(symmatu(omega.rows(0, n-1) - Urow.cols(0, n*qd - 1) * Urow.cols(0, n*qd - 1).t()), "lower");
    U = join_cols(U, Urow);
    Uinv = join_cols(Uinv, inv(Urow.cols(n*qd, n*(qd+1) - 1)));
    i+=1;
    dU = arma::abs(U.rows(U.n_rows - 2*n, U.n_rows - n - 1) - Urow);
  }
}

// Computes (minus) the log likelihood using the exact discret time representation
double carmaModel::LLK(const arma::vec bFull) {
//  std::clock_t start = std::clock();
  const unsigned int qd = p - (nf==0);
  unsigned int i, j;
  double lmm;
  
  arma::mat bigF = zeros(n, n*p);
  arma::mat f;
  arma::mat covv = zeros(n*(qd + 1), n);
  cppEDRh(bFull, bigF, f, covv);
//  std::clock_t EDRh = std::clock();
  arma::mat fm(n, n*(p+1));
  fm.submat(0,0,n-1,n-1).eye();
  fm.submat(0,n,n-1,n*(p+1)-1) = -bigF;
  arma::mat eta = fm*x;
  for (i = 0; i<Time; i++) {
    for (j = 0; j<n; j++) {
      eta(j, i) -= f(j);
    }
  }
  
  // Choleski
  arma::mat U = zeros(n, n*(qd+1));
  arma::mat Uinv(n, n);
  blockCholCpp(covv, Time, U, Uinv);
  // double test=0;
  //Defines Epsilon
  arma::vec eps(n*Time);
  eps(span(0, n-1)) = Uinv.rows(0, n-1) * eta.col(0);
  lmm = sum(log(diagvec(U.submat(0, n*qd, n - 1, n*(qd+1) - 1)))) + 0.5*std::pow(norm(eps(span(0, n-1)), 2), 2);
  // test+=sum(log(diagvec(U.submat(0, n*qd, n - 1, n*(qd+1) - 1))));
  for(i=1; i<qd+1; i++) {
    eps(span(n*i, n*(i+1) - 1)) = Uinv.rows(n*i, n*(i+1) - 1) * (eta.col(i) - U.submat(i*n, (qd-i)*n, (i+1)*n - 1, qd*n - 1)*eps(span(0, n*i - 1)));
    lmm += sum(log(diagvec(U.submat(i*n, n*qd, (i+1)*n - 1, n*(qd+1) - 1)))) + 0.5*std::pow(norm(eps(span(n*i, n*(i+1) - 1)), 2), 2);
    // test+=sum(log(diagvec(U.submat(i*n, n*qd, (i+1)*n - 1, n*(qd+1) - 1))));
  }
  for(i=qd+1; i<U.n_rows/n; i++) {
    eps(span(n*i, n*(i+1) - 1)) = Uinv.rows(n*i, n*(i+1) - 1) * (eta.col(i) - U.submat(i*n, 0, (i+1)*n - 1, qd*n - 1)*eps(span(n*(i-qd), n*i - 1)));
    lmm += sum(log(diagvec(U.submat(i*n, n*qd, (i+1)*n - 1, n*(qd+1) - 1)))) + 0.5*std::pow(norm(eps(span(n*i, n*(i+1) - 1)), 2), 2);
    // test+= sum(log(diagvec(U.submat(i*n, n*qd, (i+1)*n - 1, n*(qd+1) - 1))));
  }
  double lastChol = U.n_rows/n;
  for (i = lastChol; i<Time; i++) {
    if (qd==0) {
      eps(span(n*i, n*(i+1) - 1)) = Uinv.rows(0, n*lastChol - 1) * eta.col(i);
    }
    else {
      eps(span(n*i, n*(i+1) - 1)) = Uinv.rows(n*(lastChol-1), n*lastChol - 1) * (eta.col(i) - U.submat((lastChol - 1)*n, 0, n*lastChol - 1, qd*n - 1)*eps(span(n*(i - qd), n*i - 1)));
    }
    lmm += sum(log(diagvec(U.submat((lastChol - 1)*n, n*qd, lastChol*n - 1, n*(qd+1) - 1)))) + 0.5*std::pow(norm(eps(span(n*i, n*(i+1) - 1)), 2), 2);
  }
  lmm+=n*Time/2*log(2*M_PI);
  return lmm;
}

double carmaModel::Evaluate(const arma::vec bFull) {
  double out;
  try{
    out = LLK(bFull);
  }
  catch(...) {
    out=1e100;
  }
  return out;
}

double carmaModel::penalize(const arma::vec bFull, const arma::vec bFullInit) {
  return 0.2*n*Time*arma::norm(bFullInit - bFull, 2);
}

// Defines the matrix S1, such that the stock variables are first.
arma::mat carmaModel::createS1() {
  arma::mat S1 = zeros(n, n+r);
  unsigned int i;
  if (ns>0) {
    // S1.submat(0, nf, ns-1, n-1) = speye(ns, ns);
    
    for(i=0; i<ns; i++) {
      S1(stocks(i), stocks(i) + nf) = 1;
    }
  }
  if (nf>0) {
    // S1.submat(ns, 0, n-1, nf-1) = speye(nf, nf)/h;
    
    for(i=0; i<nf; i++) {
      S1(flows(i), i) = 1;///h;
    }
  }
  return S1;
}

// Defines the matrix S2, such that the stock variables are first.
arma::mat carmaModel::createS2() {
  unsigned int i;
  arma::mat S2 = zeros(r, n+r);
  
  if (r>0) {
    // S2.submat(0, n, r-1, n+r-1) = speye(r, r);
    if (nf>0) {
      for (i=0; i<nf; i++) {
        S2(i, nf + flows(i)) = 1;
      }
    }
    if(r>nf) {
      S2.submat(nf, n + nf, r - 1, n+r - 1).eye();
    }
  }
  else {
    S2 = zeros(1,1);
  }
  return S2;
}

// Define the matrix N from Thornton and Chambers (2017).
arma::mat carmaModel::createN(arma::mat & c11, arma::mat & c21) {
  arma::mat bigN = zeros(m*(r + n), (m+1)*n);
  bigN.submat(0, 0, m*n-1, m*n - 1) = -eye(m*n, m*n);
  
  for (unsigned int i=0; i<m; i++) {
    bigN.submat(i*n, (i+1)*n, (i+1)*n - 1, (i+2)*n - 1) = c11;
    bigN.submat(m*n + i*r, (i+1)*n, m*n + (i+1)*r - 1, (i+2)*n - 1) = c21;
  }
  return bigN;
}

// Define the matrix N from Thornton and Chambers (2017).
// Does not invert it or multiply it by T as in the original R code, which is made in the EDRh function
arma::mat carmaModel::createM(arma::mat & c12, arma::mat & c22) {
  arma::mat bigM = zeros(m*(r+n), r*(m+1));
  bigM.submat(m*n, 0, m*(n + r) - 1, m*r - 1) = eye(m*r, m*r);
  for (unsigned int i=0; i<m; i++) {
    bigM.submat(n*i, r*(1+i), n*(i+1) - 1, r*(2+i) - 1) = -c12;
    bigM.submat(m*n + i*r, (i+1)*r, m*n + (i+1)*r - 1, (i+2)*r - 1) = -c22;
  }
  return bigM;
}

// Define the matrix R (named T in the code) from Thornton and Chambers (2017). 
//It is the matrix that spans the nullspace of N
arma::mat carmaModel::createT(arma::mat & c11,
                              arma::mat & c21) {
  arma::mat bigT = zeros((p+1)*r, p*(r+n));
  arma::mat bothC(n + r, ns);
  if (ns>0) {
    bothC.submat(0, 0, n - 1, ns - 1) = c11.cols(stocks);
    bothC.submat(n, 0, n+r - 1, ns - 1) = c21.cols(stocks);
  }
  arma::mat nullMat = null(bothC.t(), 1e-12).t();
  
  if(p==1) {
    bigT = nullMat;
  }
  
  else {
    bigT.submat(0,0, n*(p-1) - 1, n*(p-1)-1) = eye(n*(p-1), n*(p-1));
    bigT.submat(2*r, n*p, (p+1)*r - 1, p*(r+n) - r - 1) = eye((p-1)*r, (p-1)*r);
    bigT.submat(n*(p-1), n*(p-1), 2*r - 1, p*n - 1) = nullMat.cols(0,n-1);
    bigT.submat(n*(p-1), p*n + r*(p-1), 2*r - 1, p*(n+r)-1) = nullMat.cols(n , n+r-1);
  }
  return bigT;
}

// Computes the vector epsilon from from Thornton and Chambers (2017), equation (10).
arma::mat carmaModel::phiMat(const arma::mat &H) {
  unsigned int dimH = nf + n*p;
  arma::mat e = eye(dimH, dimH);
  arma::mat de = eye(dimH, dimH);
  unsigned int i = 2;
  while(de.max()>1e-10 && i<1000) {
    de = de*H/i;
    e = e + de;
    i+=1;
  }
  return e;
}

// Computes the matrix omega epsilon from from Thornton and Chambers (2017), theorem 1.
arma::mat carmaModel::omegaEpsMat(arma::mat J,
								  const unsigned int dimH,
								  const arma::uword s,
								  std::vector<arma::mat>& allPowH, 
								  std::vector<arma::mat>& allPowExpAoverS) {

	/*******************************************/
	/************** ORIGINAL CODE **************/
	/*******************************************/
/*
//  std::clock_t begin = std::clock();
  arma::mat H = allPowH[0]*pow(2, s);
//  const unsigned int dimH = n*p + nf;
  arma::mat Mstar = zeros(2*dimH, 2*dimH);
  Mstar.submat(0, 0, dimH - 1, dimH - 1) = -H;
  Mstar.submat(dimH, dimH, 2*dimH - 1, 2*dimH - 1) = H.t();
  Mstar.submat(0, dimH, dimH - 1, 2*dimH - 1) = J;//theta * sigma * theta.t();
//  std::clock_t endInit = std::clock();  
  Mstar = expmat(Mstar);
//  std::clock_t expmat = std::clock();  
  arma::mat out = symmatu(Mstar.submat(dimH, dimH, 2*dimH - 1, 2*dimH - 1).t() * Mstar.submat(0, dimH, dimH - 1, 2*dimH - 1));
  std::clock_t end = std::clock();
//  Rcpp::Rcout<<"omegaExpMat Computation Time, init "<<(endInit-begin)<<" expMat "<<(expmat-endInit)<<" end "<<(end-expmat)<<std::endl;
*/
	/******************************************/
	/************* OPTIMIZED CODE *************/
	/******************************************/

//	arma::mat Mstar = zeros(2*dimH, 2*dimH);
//	arma::mat J = theta * sigma * theta.t();
//	const double normVal = std::max(arma::norm(H, "inf"), arma::norm(J, "inf"));
//	const double logVal = (normVal > 0) ? double(std::log(normVal)) : double(0); // log because we'll take exponential of the matrix
//	int exponent = 0;  std::frexp(logVal, &exponent);
//	const arma::uword s = arma::uword( (std::max)(0, exponent + 5) );
	unsigned int N = 7;	
	J/=pow(2, s);
	double c = 1;
//	arma::mat Htilde2Plus = arma::eye(dimH, dimH) + H;
	arma::mat Htilde2Minus = arma::eye(dimH, dimH) - allPowH[0];
	arma::mat Jtilde2 = J;
//	arma::mat Htilde = H;
	arma::mat Jtilde = J;
	bool positive = true;
	for (unsigned int i=2; i<=N; ++i) { //Taylor Approximation of Mstar/2^s
		c*=i; // c=i!
		if (positive) {Jtilde = -allPowH[i-2]*J + Jtilde*allPowH[0].t();} else {Jtilde = allPowH[i-2]*J + Jtilde*allPowH[0].t();}
		if (positive) {Htilde2Minus += allPowH[i-1]/c;} else {Htilde2Minus -= allPowH[i-1]/c; }
		//Htilde2Plus += allPowH[i-1]/c;
		Jtilde2 += Jtilde/c;
		positive = (positive) ? false : true; 
	}
//	Htilde2Plus = Htilde2Plus.t();
	for (unsigned int i=1;i<=s; ++i) { //Square Rq(Mstar/2^s) s times to recover our approximation of exp(Mstar)
		Jtilde2 = Htilde2Minus*Jtilde2 + Jtilde2*allPowExpAoverS[i-1].t();
//		Htilde2Plus *= Htilde2Plus;
		Htilde2Minus *= Htilde2Minus;
	}
//	Mstar.submat(0, 0, dimH-1, dimH-1) = Htilde2Minus;
//	Mstar.submat(dimH, dimH, 2*dimH-1, 2*dimH-1) = Htilde2Plus;
//	Mstar.submat(0, dimH, dimH-1, 2*dimH-1) = Jtilde2;
//	arma::mat out = symmatu(Mstar.submat(dimH, dimH, 2*dimH - 1, 2*dimH - 1).t() * Mstar.submat(0, dimH, dimH - 1, 2*dimH - 1));

	arma::mat out = symmatu(allPowExpAoverS[s] * Jtilde2);

	return out;
}


// Optimal computation of exp(H) and save intermediary results for the computation of omegaEpsMat
arma::mat carmaModel::myExpmatForH(arma::mat J,
								   const unsigned int dimH,
								   const arma::uword s,
								   std::vector<arma::mat>& allPowH,
								   std::vector<arma::mat>& allPowExpAoverS) {
	unsigned int N = 7;
	double c = 1;
	std::vector<arma::mat> allPowA(N, arma::mat(n*p, n*p));
	allPowExpAoverS[0] = arma::eye(dimH, dimH) + allPowH[0];
	allPowA[0] = allPowH[0].submat(nf, nf, dimH-1,dimH-1);
	for (unsigned int i=2; i<=N; ++i) {
		allPowA[i-1] = allPowA[i-2]*allPowA[0];
	}


	for (unsigned int i=2; i<=N; ++i) {
		c*=i;
//		allPowH[i-1] = allPowH[i-2]*allPowH[0];
		if (nf>0) {// if there is at least one flow
			allPowH[i-1].cols(0, nf-1).zeros();
			allPowH[i-1].rows(0, nf-1).zeros();
			for (unsigned int j=0; j<nf; ++j) {
				allPowH[i-1](j, span(nf, dimH-1)) = allPowA[i-2].row(flows(j))/pow(2, s);
		    }
		}
		allPowH[i-1].submat(nf, nf, dimH-1, dimH-1) = allPowA[i-1];
		allPowExpAoverS[0]+=allPowH[i-1]/c;
	}
	for (unsigned int i=1; i<=s; ++i) { 
		allPowExpAoverS[i] = allPowExpAoverS[i-1]*allPowExpAoverS[i-1];
	}
	return allPowExpAoverS[s];
}

arma::mat carmaModel::dataBlockCpp(const arma::mat& xTh) {
  arma::mat out(xTh.n_rows-p, xTh.n_cols*(p+1));
  // out.cols(0, xTh.n_cols-1) = xTh(span(p, xTh.n_row-1), span(0, xTh.n_cols-1));
  for (unsigned int i=0;i<p+1; i++) {
    out.cols(xTh.n_cols*i, xTh.n_cols*(i+1)-1) = xTh(span(p-i, xTh.n_rows-1-i), span(0, xTh.n_cols-1));
  }
  return out.t();
}
