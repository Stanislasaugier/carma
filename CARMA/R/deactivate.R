deactivate <- function(x, n, nActivated = NULL, diagMat=FALSE) {
  if (is.null(nActivated)) {
    nActivated <- nrow(x)*ncol(x)/2
  }
  
  if(diagMat==FALSE) {
    compt <- length(which(x!=0)) - length(which(diag(x)!=0)) # number of activated (non diagonal) elements at any iteration
    ij <- sample.int(n, 2)
    while(compt>nActivated) {
      while (x[ij[1], ij[2]]==0) {
        ij <- sample.int(n, 2)
      }
      x[ij[1], ij[2]] <- 0
      compt = compt-1
    }
  } else {
    compt <- length(which(x[upper.tri(x, diag=FALSE)]!=0)) # number of activated (non diagonal) elements at any iteration
    ij <- sample.int(n, 2)
    while(compt>nActivated) {
      while (ij[1]>ij[2] || x[ij[1], ij[2]]==0) {
        ij <- sample.int(n, 2)
      }
      x[ij[1], ij[2]] <- 0
      compt = compt-1
    }
  }
  return(x)
}